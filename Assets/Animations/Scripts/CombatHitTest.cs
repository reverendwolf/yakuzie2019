﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatHitTest : MonoBehaviour
{
    CombatActor actor;
    Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.K))
        {
            if(actor == null)
            {
                actor =  GetComponent<CombatActor>();
                anim = actor.Animator;
            }
            int r = Random.Range(1, 6);
            anim.SetInteger("HitSelector", r);
            anim.SetTrigger("OnHit");
        }
    }
}
