﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinnedBakeTest : MonoBehaviour
{
    public SkinnedMeshRenderer skin;
    public MeshFilter target;
    public MeshRenderer targetRenderer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyUp(KeyCode.M))
        {
            skin.BakeMesh(target.mesh);
            StopAllCoroutines();
            StartCoroutine(Fade());
        }
    }

    IEnumerator Fade()
    {
        targetRenderer.enabled = true;
        Material m = targetRenderer.material;
        m.color = Color.white;
        while(m.color.a >= 0f)
        {
            yield return new WaitForEndOfFrame();
            Color fade = new Color(m.color.r, m.color.g, m.color.b, m.color.a - (Time.deltaTime * 6));
            m.color = fade;
        }
        targetRenderer.enabled = false;
    }
}
