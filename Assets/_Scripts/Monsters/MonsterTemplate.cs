﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class MonsterTemplate : ScriptableObject
{
    [SerializeField]
    protected MonsterBasicInfo basicInfo;
    public MonsterBasicInfo BasicInfo { get { return basicInfo; } }

    [SerializeField]
    List<MoveDamageType> weak;
    public List<MoveDamageType> Weak { get { return weak; } }
    [SerializeField]
    List<MoveDamageType> resist;
    public List<MoveDamageType> Resist { get { return resist; } }
    [SerializeField]
    List<MoveDamageType> immune;
    public List<MoveDamageType> Immune { get { return immune; } }

    public MonsterTemplate()
    {
        basicInfo = new MonsterBasicInfo();
    }
}