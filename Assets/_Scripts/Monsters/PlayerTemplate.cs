﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class PlayerTemplate : MonsterTemplate
{
    [SerializeField]
    string hpVirtue;
    public string HPVirtue { get { return hpVirtue; } }
    [SerializeField]
    string apVirtue;
    public string APVirtue { get { return apVirtue; } }
    [SerializeField]
    string atkVirtue;
    public string ATKVirtue { get { return atkVirtue; } }
    [SerializeField]
    string expVirtue;
    public string EXPVirtue { get { return expVirtue; } }
    [SerializeField]
    string gpVirtue;
    public string GPVirtue { get { return gpVirtue; } }
    [SerializeField]
    string specialVirtue;
    public string SpecialVirtue { get { return specialVirtue; } }

    public PlayerTemplate()
    {
        basicInfo = new MonsterBasicInfo();
    }
}
