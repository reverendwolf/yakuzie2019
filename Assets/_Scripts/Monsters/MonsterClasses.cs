﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MonsterBasicInfo
{
    [SerializeField] string displayName;
    public string DisplayName { get { return displayName; } }

    [SerializeField] [TextArea] string description;
    public string Description { get { return description; } }

    [SerializeField] int startingHP = 1;
    public int StartingHP { get { return startingHP; } }

    [SerializeField] int bonusHP;
    public int BonusHP { get { return bonusHP; } }
}

[System.Serializable]
public class MonsterMoveUpgrade
{
    [SerializeField]
    string name;
    [SerializeField]
    [TextArea]
    string description;

    [SerializeField]
    CombatMoveBase move;


    [SerializeField]
    int cost;
    [SerializeField]
    bool unleashed;

    public string Name { get { return name; } }
    public string Description { get { return description; } }
    public CombatMoveBase Move { get { return move; } }
    public int Cost { get { return cost; } }
    public bool Unleashed { get { return unleashed; } }
}

[System.Serializable]
public class MonsterDeathblowUpgrade : MonsterMoveUpgrade
{
    public enum DeathblowList
    {
        LS,
        LLS,
        MS,
        LLLS,
        LMS,
        MLS,
        SS,
        LLLLS,
        LLMS,
        LMLS,
        MLLS,
        MMS,
        SLS
    }

    [SerializeField]
    DeathblowList slot;
    public DeathblowList Slot { get { return slot; } }
}

[System.Serializable]
public class MonsterPersonality
{
    [SerializeField] [Range(0, 1f)] float aggression;
    public float Aggression { get { return aggression; } }
    [SerializeField] [Range(0, 1f)] float morale;
    public float Morale { get { return morale; } }
    [SerializeField] [Range(1, 10f)] float optimalRange;
    public float OptimalRange { get { return optimalRange; } }
}

[System.Serializable]
public class MonsterStatSheet
{
    public enum Virtues
    {
        HP, AP, ATK, EXP, GP
    }

    public bool[] dbUnlocked = new bool[8];
    public bool[] techUnlocked = new bool[10];
    [Range(0, 5)] public int hp = 0;
    [Range(0, 5)] public int ap = 0;
    [Range(0, 5)] public int atk = 0;

    public bool expBoost;
    public bool gpBoost;
    public bool unleashed;

    public void UpgradeVirtue(Virtues virtue)
    {
        switch (virtue)
        {
            case Virtues.HP:
                hp = Mathf.Clamp(hp + 1, 0, 5);
                break;
            case Virtues.AP:
                ap = Mathf.Clamp(ap + 1, 0, 5);
                break;
            case Virtues.ATK:
                atk = Mathf.Clamp(atk + 1, 0, 5);
                break;
            case Virtues.EXP:
                expBoost = true;
                break;
            case Virtues.GP:
                gpBoost = true;
                break;
        }
    }

    public void Unleash()
    {
        unleashed = true;
    }

    public int curHP;
    public int curSP;
}

[System.Serializable]
public class MonsterBattleData
{
    public delegate void BattleEvent();
    public BattleEvent OnChangeCT = delegate { };

    [System.Serializable]
    public class DamageSlot
    {
        public MoveDamageType damage;
        public float timeRemaining;
    }

    const float RANDOMCTMAX = 0.4f;
    public enum Stance { Normal, Weak, KO }
    [SerializeField]
    float ct;
    public float CT { get { return ct; } }
    public bool CTFull { get { return (ct >= 1f); } }
    [SerializeField]
    int ap;
    public int AP { get { return ap; } set { ap = value; } }
    [SerializeField]
    Stance stance;
    public Stance BattleStance { get { return stance; } set { stance = value; } }
    [SerializeField]
    List<DamageSlot> weak = new List<DamageSlot>();
    [SerializeField]
    List<DamageSlot> resist = new List<DamageSlot>();
    [SerializeField]
    List<DamageSlot> immune = new List<DamageSlot>();

    public bool IsWeak(MoveDamageType type)
    {
        return ListHasType(weak, type);
    }

    public bool IsResist(MoveDamageType type)
    {
        return ListHasType(resist, type);
    }

    public bool IsImmune(MoveDamageType type)
    {
        return ListHasType(immune, type);
    }

    public int UseAP()
    {
        int r = ap;
        ap = 0;
        return r;
    }

    void RunSlotList(List<DamageSlot> list, float delta)
    {
        for (int i = 0; i < list.Count; i++)
        {
            list[i].timeRemaining -= delta;
            if(list[i].timeRemaining <= 0f)
            {
                list.RemoveAt(i);
                i--;
            }
        }
    }

    bool ListHasType(List<DamageSlot> list, MoveDamageType type)
    {
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i].damage == type)
                return true;
        }
        return false;
    }

    public void ChargeCT(float delta, float modifier = 0f)
    {
        if (stance == Stance.KO)
            return;
        ct += delta + modifier;
        if (ct > 1f)
        {
            ct = 1f;
        }
        RunSlotList(weak, delta);
        RunSlotList(resist, delta);
        RunSlotList(immune, delta);
        OnChangeCT();
    }

    public void ClearCT()
    {
        ct = 0f;
        OnChangeCT();
    }

    public void SetRandomCT()
    {
        ct = Random.value * RANDOMCTMAX;
        OnChangeCT();
    }
}
