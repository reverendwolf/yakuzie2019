﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Fungus.MenuDialog))]
public class MenuDialogueInterface : MonoBehaviour {

    Fungus.MenuDialog md;
    [SerializeField]
    int selectedIndex = 0;

	// Use this for initialization
	void Start () {
        md = GetComponent<Fungus.MenuDialog>();
        GetComponent<Canvas>().enabled = true;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnEnable()
    {
        InputMaster.OnActionPressed += ButtonInput;
    }

    private void OnDisable()
    {
        InputMaster.OnActionPressed -= ButtonInput;
    }

    public void ButtonInput(InputMaster.ButtonInput button, InputMaster.ButtonModifier modifier)
    {
        if (md.DisplayedOptionsCount <= 0)
            return;

        bool selectInput = false;
        if(button == InputMaster.ButtonInput.Up)
        {
            selectedIndex--;
            selectInput = true;
        }
        else if(button == InputMaster.ButtonInput.Down)
        {
            selectedIndex++;
            selectInput = true;
        }

        if(selectInput)
        {
            selectedIndex = Mathf.Clamp(selectedIndex, 0, md.DisplayedOptionsCount - 1);
            md.CachedButtons[selectedIndex].Select();
        }
        
        if(button == InputMaster.ButtonInput.Cross)
        {
            md.CachedButtons[selectedIndex].onClick.Invoke();
            selectedIndex = 0;
        }
    }
}
