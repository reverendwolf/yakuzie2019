﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

[CommandInfo("Narrative", "Actor Emote", "Select an actor to pop an emote")]
public class CommandActorEmote : Command
{
    public bool waitWhileEmote;
    public NPCActor actor;
    public ActorEmote.Emote emote;

    public override void OnEnter()
    {
        if(actor != null)
        {
            actor.Emote.PopEmote(emote);
        }

        if (waitWhileEmote) Invoke("Continue", ActorEmote.EMOTEWAIT);
        else Continue();

    }

    public override string GetSummary()
    {
        return (actor != null ? actor.name : "<No Actor>") + " pops " + emote.ToString(); 
    }

    public override Color GetButtonColor()
    {
        return new Color(0.65f, 1f, 0.65f);
    }
}
