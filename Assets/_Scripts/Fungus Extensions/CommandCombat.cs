﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

[CommandInfo("RPG", "Combat Encounter", "Begins a fight  minigame, continues the block")]
public class CommandCombat : Command
{
    //CombatMachine CM;
    VSCombatMachine CM;

    public string combatName;

    public VSCombatStage assignedStage;
    
    // Use this for initialization
    public override void OnEnter()
    {
        CM = GameObject.FindObjectOfType<VSCombatMachine>();
        //Invoke("Continue", 1.5f);
        if(CM != null)
        {
            //CombatMachine.OnBattleEnd += CombatComplete;
            VSCombatMachine.OnCombatEnd += CombatComplete;
            if (assignedStage != null && assignedStage.GetEnemies().Length > 0)
                CM.StartCombat(0, assignedStage.GetEnemies(), combatName, assignedStage);
            else
            {
                Debug.LogWarning("Combat missing required elements.");
                CombatComplete(1);
            }
        }
        else
        {
            Debug.LogWarning("CombatMachine Missing");
        }
        
    }

    void CombatComplete(int battleCode)
    {
        //CombatMachine.OnBattleEnd -= CombatComplete;
        VSCombatMachine.OnCombatEnd -= CombatComplete;
        Debug.Log("Fungus Combat Complete: " + battleCode);
        GetFlowchart().SetIntegerVariable("BattleResult", battleCode);
        Continue();
    }

    public override string GetSummary()
    {
        if (assignedStage == null)
            return "Please add a stage for this combat";
        else
            return "Combat vs " + assignedStage.GetEnemies().Length + ( assignedStage.GetEnemies().Length == 1 ? " enemy." : " enemies.");
    }

    public override Color GetButtonColor()
    {
        return new Color32(210, 100, 100, 255);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
