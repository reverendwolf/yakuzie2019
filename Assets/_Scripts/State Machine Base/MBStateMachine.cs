﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class MBStateMachine : MonoBehaviour
{
    public delegate void OnStateChanged(MBBaseState newState);
    public OnStateChanged onStateChanged;

    public delegate void IntegerSignal(int signal);

    public virtual MBBaseState CurrentState
    {
        get { return currentState; }
        set { Transition(value); }
    }

    MBBaseState currentState;
    bool inTransition;

    public virtual T GetState<T>() where T : MBBaseState
    {
        T target = GetComponent<T>();
        if (target == null)
        {
            target = gameObject.AddComponent<T>();
        }
        return target;
    }

    public virtual void ChangeState<T>() where T : MBBaseState
    {
        CurrentState = GetState<T>();
    }

    protected virtual void Transition(MBBaseState newState)
    {
        if (currentState == newState || inTransition)
            return;

        inTransition = true;

        if (currentState != null)
            currentState.Exit();

        currentState = newState;

        if (currentState != null)
        {
            currentState.Enter();
            if (onStateChanged != null)
                onStateChanged(CurrentState);
        }

        inTransition = false;
    }

    private void Update()
    {
        if (inTransition == false && currentState != null) currentState.Tick();
    }
}
