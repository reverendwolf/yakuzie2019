﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class MBBaseState : MonoBehaviour
{
    public virtual void Enter()
    {
        AddListeners();
    }

    public virtual void Exit()
    {
        RemoveListeners();
    }

    public virtual void OnDestroy()
    {
        RemoveListeners();
    }

    public virtual void AddListeners()
    {

    }

    public virtual void RemoveListeners()
    {

    }

    public virtual void Tick()
    {

    }

}
