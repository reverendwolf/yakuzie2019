﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GMUIVitals : MonoBehaviour
{
    public Slider hpSlider;
    public GameObject[] mpBars;
    Image[] mpFillers;
    public Image ctFiller;

    PlayerActor player;

    public void Initialize(PlayerActor player)
    {
        if(mpBars.Length <= 0)
        {
            Debug.LogError("NO MP BARS IN GM UI");
        }
        mpFillers = new Image[mpBars.Length];
        for (int i = 0; i < mpBars.Length; i++)
        {
            mpFillers[i] = mpBars[i].transform.GetChild(0).GetComponent<Image>();
        }

        this.player = player;
        player.OnBattleUpdate += UpdateVitals;
        player.BattleData.OnChangeCT += UpdateCT;
        UpdateVitals();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable()
    {
        if (player != null) player.OnBattleUpdate += UpdateVitals;
        if (player != null) player.BattleData.OnChangeCT += UpdateCT;
    }

    private void OnDisable()
    {
        if (player != null) player.OnBattleUpdate -= UpdateVitals;
        if (player != null) player.BattleData.OnChangeCT -= UpdateCT;
    }

    public void UpdateVitals()
    {
        hpSlider.value = player.CurHPPercent;
        

        for (int i = 0; i < mpBars.Length; i++)
        {
            if(i <= player.MaxSPBars - 1)
            {
                mpBars[i].SetActive(true);
                if(i <= player.CurSPBars - 1)
                {
                    mpFillers[i].fillAmount = 1;
                }
                else if (i == player.CurSPBars)
                {
                    mpFillers[i].fillAmount = player.CurSPPercent;
                }
                else
                {
                    mpFillers[i].fillAmount = 0;
                }
            }
            else
            {
                mpBars[i].SetActive(false);
            }
        }
    }

    public void UpdateCT()
    {
        ctFiller.fillAmount = player.BattleData.CT;
    }

}
