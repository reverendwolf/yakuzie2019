﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMasterSettings : MonoBehaviour
{
    [System.Serializable]
    public struct ItemData
    {
        public CombatMoveBase item;
        public int value;
        public int stack;
    }

    [SerializeField]
    float hitStopDuration;
    public float HitStopDuration { get { return hitStopDuration; } }

    [SerializeField]
    float hitSlowDuration;
    public float HitSlowDuration { get { return hitSlowDuration; } }

    [SerializeField]
    float hitSlowSpeed;
    public float HitSlowSpeed { get { return hitSlowSpeed; } }

    [SerializeField]
    ItemData[] itemRegistry;
    public ItemData[] ItemRegistry { get { return itemRegistry; } }
    CombatMoveBase[] itemDatabase;
    public CombatMoveBase[] ItemDatabase { get { return itemDatabase; } }

    private void OnEnable()
    {
        CreateItemDatabase();
    }

    void CreateItemDatabase()
    {
        itemDatabase = new CombatMoveBase[itemRegistry.Length];
        for (int i = 0; i < itemDatabase.Length; i++)
        {
            itemDatabase[i] = itemRegistry[i].item;
        }
    }

    public int GetItemIndex(CombatMoveBase item)
    {
        for (int i = 0; i < itemDatabase.Length; i++)
        {
            if (item == itemDatabase[i])
                return i;
        }
        return -1;
    }
}
