﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(GameMasterSettings))]
public class GameMasterMachine : MBStateMachine
{

    private static GameMasterMachine _instance;
    public static GameMasterMachine I
    {
        get
        {
            if (!_instance)
            {
                _instance = GameObject.FindObjectOfType(typeof(GameMasterMachine)) as GameMasterMachine;
            }

            if (!_instance)
            {
                Debug.LogError("There is no GameMasterMachine object!");
            }

            return _instance;
        }
    }

    public void OnEnable()
    {
        if (I != null && I != this)
            DestroyImmediate(this.gameObject);
    }

    public delegate void Pause(bool pause);
    public static Pause PauseWorld;

    public delegate void AreaDataEvent(AreaData area);
    public static AreaDataEvent OnAreaTransition;

    [SerializeField]
    string serializedObject;

    GameMasterSettings settings;
    public GameMasterSettings Settings { get { if (settings == null) settings = GetComponent<GameMasterSettings>(); return settings; } }
    GameMasterUI ui;
    public GameMasterUI UI { get { if (ui == null) ui = GetComponent<GameMasterUI>(); return ui; } }

    public GameObject PlayerObject { get { return mission.PlayerObject; } }
    public PlayerActor PlayerActor { get { return PlayerObject.GetComponent<PlayerActor>(); } }

    [SerializeField]
    GameData gameData;
    public GameData GameData { get { return gameData; } }

    [SerializeField]
    Image colorFade;

    //public PlayerData[] PlayerParty { get { return gameData.playerParty; } }
    [SerializeField] MissionObject mission;
    public MissionObject Mission { get { return mission; } }
    public Fungus.Flowchart Flowchart { get { return mission.Flowchart; } }

    //public GameObject DungeonCamera { get { return dungeonCamera.gameObject; } }

    [SerializeField]
    Cinemachine.CinemachineVirtualCamera dungeonCamera;

    Coroutine areaTransition;

    bool cameraFocused;

    public bool CameraFocused { get { return cameraFocused; } }

    [SerializeField]
    VSMPlayerControls playerControls;
    public VSMPlayerControls Controls { get { return playerControls; } }

    bool hitStop = false;
    bool hitSlow = false;

    private void Start()
    {

        //Initialize();
        //focusOrbit = focusCamera.GetCinemachineComponent<Cinemachine.CinemachineOrbitalTransposer>();
        //Debug.Log(JsonUtility.ToJson(gameData, true));
    }


    void Initialize()
    {

    }


    public void RegisterMission(MissionObject mission)
    {
        this.mission = mission;
        dungeonCamera.Follow = PlayerObject.transform;
        dungeonCamera.LookAt = PlayerObject.transform;
        ChangeState<GMWalk>();

    }

    public void AreaTransition(MapTeleport teleport)
    {
        if (areaTransition == null)
            areaTransition = StartCoroutine(DoAreaTransition(teleport));
    }

    IEnumerator DoAreaTransition(MapTeleport teleport)
    {
        yield return UI.ColorFade(Color.black);
        AreaData exit = teleport.ParentArea;
        AreaData next = teleport.Link.ParentArea;
        next.gameObject.SetActive(true);
        //GM.playerObject.transform.position = ((MapTeleport)mo).ExitPoint.position;
        //yield return StartCoroutine(GM.TeleportPlayer(((MapTeleport)mo).Exit.position));
        TeleportPlayer(teleport.Link.Exit.position);
        exit.gameObject.SetActive(false);
        if (OnAreaTransition != null) OnAreaTransition(next);
        yield return UI.FadeIn();
        ChangeState<GMWalk>();
        areaTransition = null;
    }

    public void TeleportPlayer(Vector3 newPosition)
    {
        Vector3 cameraOffset = PlayerObject.transform.position - dungeonCamera.transform.position;
        dungeonCamera.GetComponent<Cinemachine.CinemachineVirtualCameraBase>().enabled = false;
        dungeonCamera.transform.position = newPosition - cameraOffset;
        //playerObject.transform.position = newPosition;
        PlayerActor.Teleport(newPosition);
        dungeonCamera.GetComponent<Cinemachine.CinemachineVirtualCameraBase>().enabled = true;
        //yield return new WaitForSeconds(0.5f);
    }

    public void EnablePlayerObject(bool enable)
    {
        PlayerObject.SetActive(enable);
    }

    public void PauseTheWorld(bool pause)
    {
        if (PauseWorld != null) PauseWorld(pause);
    }

    public void HitStop()
    {
        if (hitStop)
            return;

        StartCoroutine(DoHitEvent(0, Settings.HitStopDuration));
    }

    public void HitSlow()
    {
        if (hitSlow)
            return;

        StartCoroutine(DoHitEvent(Settings.HitSlowSpeed, Settings.HitSlowDuration));
    }

    IEnumerator DoHitEvent(float timeSlow, float duration)
    {
        timeSlow = Mathf.Clamp01(timeSlow);
        float timeScale = Time.timeScale;
        if (timeScale == 0f) timeScale = 1f;
        if (timeSlow <= 0)
            hitStop = true;
        else
            hitSlow = true;

        Time.timeScale = timeSlow;
        yield return new WaitForSecondsRealtime(duration);
        Time.timeScale = timeScale;
        hitSlow = hitStop = false;
    }

    public void ShowInteractionLabel(string verb)
    {
        playerControls.SetInteractionLabel(verb);

        //interactionLabelText.text = verb.ToString();
        //interactionLabelObject.SetActive(true);
    }

    public void HideInteractionLabel()
    {
        playerControls.HideInteractionLabel();
        //interactionLabelObject.SetActive(false);
    }

    public void ShowControls(bool hide)
    {
        playerControls.gameObject.SetActive(hide);
    }
}
