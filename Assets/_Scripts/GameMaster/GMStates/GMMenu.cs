﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GMMenu : GMBaseState
{
    public override void Enter()
    {
        base.Enter();
        Debug.Log("Open MENU");
    }

    public override void Exit()
    {
        base.Exit();
        Debug.Log("Close MENU");
    }

    public override void AddListeners()
    {
        base.AddListeners();
        InputMaster.OnActionPressed += ActionInput;
    }

    public override void RemoveListeners()
    {
        base.RemoveListeners();
        InputMaster.OnActionPressed -= ActionInput;
    }

    void ActionInput(InputMaster.ButtonInput button, InputMaster.ButtonModifier modifier)
    {
        if(button == InputMaster.ButtonInput.Circle)
        {
            GM.ChangeState<GMWalk>();
        }
    }
}
