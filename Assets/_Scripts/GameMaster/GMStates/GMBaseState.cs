﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GMBaseState : MBBaseState
{
    GameMasterMachine gm;
    protected GameMasterMachine GM { get { return gm; } }

    public void OnEnable()
    {
        gm = GetComponent<GameMasterMachine>();
        if (gm == null)
            Debug.LogWarning("GMState added where there is no GMM");
    }
}
