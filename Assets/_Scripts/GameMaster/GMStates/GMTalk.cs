﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class GMTalk : GMBaseState
{
    MapObject mo;
    Flowchart Chart { get { return GM.Flowchart; } }
    public void PassMapObject(MapObject mapObject)
    {
        this.mo = mapObject;

        if (mo.MapObjectType == MapObject.MOType.Fungus)
            HandleFungus();
        else if (mo is MapTeleport)
            HandleMapWarp(mo as MapTeleport);
        else if (mo.MapObjectType == MapObject.MOType.Pickup)
            HandleItemPickup();
        else
        {
            Debug.LogWarning("Invalid MapObjectType");
        }

    }

    public override void Exit()
    {
        base.Exit();
        mo.Trigger();
    }

    void HandleFungus()
    {
        Chart.ExecuteBlock(((FungusHandler)this.mo).FlowchartLink);
        StartCoroutine(WaitForCompletion());
    }

    IEnumerator WaitForCompletion()
    {
        //Debug.Log("Wait Start");
        while(Chart.HasExecutingBlocks())
        {
            yield return null;
        }
        //Debug.Log("Flowchart Complete");
        GM.ChangeState<GMWalk>();
    }

    void HandleMapWarp(MapTeleport teleport)
    {
        GM.AreaTransition(teleport);
    }

    void HandleItemPickup()
    {
        StartCoroutine(ItemPickup());
    }

    IEnumerator ItemPickup()
    {
        yield return null;
        ItemPickup ip = mo as ItemPickup;
        Debug.Log(GM.Settings.ItemDatabase[ip.ItemIndex].ProperName + " Get!");
        //GM.PlayerActor.DoFullAction(3);
        GM.GameData.ConsumableBag[ip.ItemIndex]++;
        GM.ChangeState<GMWalk>();
    }
}
