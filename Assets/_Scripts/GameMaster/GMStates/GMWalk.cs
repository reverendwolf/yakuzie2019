﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class GMWalk : GMBaseState
{
    const int VALIDATIONRATE = 10;
    [SerializeField] List<MapObject> activeAreas;
    CombatActor playerActor;
    int lastValidation;
    Coroutine validator;
    [SerializeField]
    bool sprinting;
    // Use this for initialization
    public override void Enter()
    {
        base.Enter();
        playerActor = GameMasterMachine.I.PlayerActor;
        //playerActor.Initialize(GM.PlayerData);
        if (activeAreas == null)
            activeAreas = new List<MapObject>();

        GM.PauseTheWorld(false);
        GM.Controls.SetContols(VSMPlayerControls.ControlsSet.Walking);
        GM.ShowControls(false);
        ValidateActiveAreas();
        validator = StartCoroutine(ValidateAutomatically());
        sprinting = false;
    }

    IEnumerator ValidateAutomatically()
    {
        while(true)
        {
            if (lastValidation >= VALIDATIONRATE)
            {
                ValidateActiveAreas();
                lastValidation = 0;
            }
            else
            {
                lastValidation++;
            }
            yield return new WaitForEndOfFrame();
        }
    }

    public override void Exit()
    {
        base.Exit();
        StopCoroutine(validator);
        GM.PauseTheWorld(true);
        GM.ShowControls(false);
        playerActor.Idle();
    }

    public override void AddListeners()
    {
        base.AddListeners();
        InputMaster.LeftStickRelativeDirection += LeftStickInput;
        InputMaster.OnActionPressed += ActionInput;
        InputMaster.OnActionReleased += ActionRelease;
        FungusHandler.PlayerEntered += PlayerEnterInteractable;
        FungusHandler.PlayerExit += PlayerExitInteractable;
    }

    public override void RemoveListeners()
    {
        base.RemoveListeners();
        InputMaster.LeftStickRelativeDirection -= LeftStickInput;
        InputMaster.OnActionPressed -= ActionInput;
        InputMaster.OnActionReleased -= ActionRelease;
        FungusHandler.PlayerEntered -= PlayerEnterInteractable;
        FungusHandler.PlayerExit -= PlayerExitInteractable;
    }

    void LeftStickInput(Vector2 direction)
    {
        if (playerActor.Busy) return;
        playerActor.WalkDirection(direction, direction.magnitude, sprinting);
    }

    void ActionInput(InputMaster.ButtonInput button, InputMaster.ButtonModifier modifier)
    {
        if(button == InputMaster.ButtonInput.Triangle)
        {
            //GM.PauseTheWorld(true);
            //GM.ChangeState<GMMenu>();
        }

        if(button == InputMaster.ButtonInput.Square)
        {
            ValidateActiveAreas();

            if(activeAreas.Count > 0)
            {
                //GM.PauseTheWorld(true);
                ActivateHandler();
            }
        }

        if(button == InputMaster.ButtonInput.L1)
        {
            sprinting = true;
            //playerActor.Idle();
            //playerActor.JumpTo(playerActor.transform.position + (playerActor.ForwardFacing * 2f));
        }

        if(button == InputMaster.ButtonInput.R1)
        {
            //playerActor.Idle();
            //playerActor.JumpBack();
        }
    }

    void ActionRelease(InputMaster.ButtonInput button, InputMaster.ButtonModifier modifier)
    {
        if(button == InputMaster.ButtonInput.L1)
        {
            sprinting = false;
        }
    }

    void ActivateHandler()
    {
        GM.ChangeState<GMTalk>();
        GM.GetComponent<GMTalk>().PassMapObject(activeAreas[activeAreas.Count - 1]);
        activeAreas.RemoveAt(0);
        playerActor.Emote.KillEmote();
    }

    void ValidateActiveAreas()
    {
        for (int i = 0; i < activeAreas.Count; i++)
        {
            if(activeAreas[0].isActiveAndEnabled == false)
            {
                activeAreas.RemoveAt(0);
                i--;
            }
        }

        if(activeAreas.Count > 0)
        {
            if(activeAreas[activeAreas.Count - 1].StartOnTrigger == false)
            {
                GM.ShowInteractionLabel(activeAreas[activeAreas.Count - 1].PromptText);
            }
            else
            {
                GM.HideInteractionLabel();
            }
                
        }
        else
        {
            GM.HideInteractionLabel();
        }
    }

    void PlayerEnterInteractable(MapObject handler)
    {
        if(activeAreas.Contains(handler) == false)
        {
            activeAreas.Add(handler);
            ValidateActiveAreas();
            //Debug.Log(handler.StartOnTrigger());
            if (handler.StartOnTrigger)
            {
                if(!(handler is MapTeleport))
                {
                    playerActor.Emote.ShowEmote(ActorEmote.Emote.Exclamation);
                }
                ActivateHandler();
            }
            else
            {
                playerActor.Emote.ShowEmote(ActorEmote.Emote.Question);
            }
            
        }
    }

    void PlayerExitInteractable(MapObject handler)
    {
        if(activeAreas.Contains(handler))
        {
            activeAreas.Remove(handler);
            ValidateActiveAreas();
            playerActor.Emote.KillEmote();
        }
        
    }
}
