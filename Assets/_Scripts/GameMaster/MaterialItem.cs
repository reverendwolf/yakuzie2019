﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu()]
public class MaterialItem : ScriptableObject
{

    [SerializeField]
    string displayName;
    public string DisplayName { get { return displayName; } }

    [TextArea]
    string description;
    public string Description { get { return description; } }

}
