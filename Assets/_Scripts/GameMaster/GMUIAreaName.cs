﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GMUIAreaName : MonoBehaviour
{
    [SerializeField] Text labelText;
    CanvasGroup cg;
    Canvas parentCanvas;
    public float waitTime = 4f;
    public float fadeTime = 1.5f;

    bool showing;
    // Start is called before the first frame update
    void Start()
    {
        cg = GetComponent<CanvasGroup>();
        parentCanvas = GetComponentInParent<Canvas>();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.R))
        {
            ShowName(GameMasterMachine.I.Mission.StartingArea);
        }
    }

    private void OnEnable()
    {
        GameMasterMachine.OnAreaTransition += ShowName;
    }

    private void OnDisable()
    {
        GameMasterMachine.OnAreaTransition -= ShowName;
    }

    public void ShowName(AreaData area)
    {
        StopAllCoroutines();
        if (area.AreaName != "")
        {
            labelText.text = area.AreaName;
        }
        else
        {
            labelText.text = area.name;
        }
        StartCoroutine(FadeOut(waitTime, fadeTime));
    }

    IEnumerator FadeOut(float wait, float time)
    {
        yield return null;
        cg.alpha = 1;
        labelText.CalculateLayoutInputHorizontal();
        yield return new WaitForSecondsRealtime(wait);
        float t = time;
        while(t >= 0f)
        {
            t -= Time.unscaledDeltaTime;
            cg.alpha = t / time;
            yield return null;
        }
        cg.alpha = 0;
    }
}
