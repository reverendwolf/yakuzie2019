﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameMasterUI : MonoBehaviour
{
    GameMasterMachine GM { get { return GameMasterMachine.I; } }
    public Image colorFade;
    public GMUIVitals playerVitals;
    // Start is called before the first frame update
    void Start()
    {
        playerVitals.Initialize(GM.PlayerActor);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Alpha4))
        {
            playerVitals.UpdateVitals();
        }
    }

    public IEnumerator ColorFade(Color color, float time = 1f)
    {
        colorFade.color = new Color(color.r, color.g, color.b, 0f);
        float timer = 0;
        while (timer <= time)
        {
            yield return new WaitForEndOfFrame();
            timer += Time.deltaTime;
            colorFade.color = new Color(colorFade.color.r, colorFade.color.g, colorFade.color.b, timer / time);
        }
    }

    public IEnumerator FadeIn()
    {
        float timer = 1f;
        while (timer >= 0f)
        {
            yield return new WaitForEndOfFrame();
            timer -= Time.deltaTime;
            colorFade.color = new Color(colorFade.color.r, colorFade.color.g, colorFade.color.b, timer);
        }
    }
}
