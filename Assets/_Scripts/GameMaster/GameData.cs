﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class GameData : ScriptableObject
{
    public enum Character
    {
        Matthiu
    }

    public const int SPBARSIZE = 14;
    public const int CTCHARGE = 35;
    public const int CTBOOST = 8;

    [SerializeField]
    int[] consumables;
    public int[] ConsumableBag { get { return consumables; } }

    [SerializeField]
    int[] materials = new int[5];
    public int[] Materials { get { return materials; } }

    [SerializeField]
    int money;
    public int Money { get { return money; } }

    MonsterStatSheet[] CharacterStats { get { return new MonsterStatSheet[] { matthiu }; } }
    [SerializeField]
    MonsterStatSheet matthiu;

    public string SaveString()
    {
        return JsonUtility.ToJson(this);
    }
    
    public bool IsCraftable(int consumableIndex)
    {
        return false;
    }

    public void ModifyMoney(int value)
    {
        money += value;
        money = Mathf.Clamp(money,0, 999);
    }

    [ContextMenu("Initialize Inventory")]
    public void InitializeInventory()
    {
        consumables = new int[7];
    }

    public MonsterStatSheet GetStats(Character character)
    {
        return CharacterStats[(int)character];
    }

}

