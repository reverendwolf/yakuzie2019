﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionObject : MonoBehaviour {

    [System.Serializable]
    public class MissionData
    {
        public bool useGameDataStats;
    }

    [SerializeField]
    MissionData missionData;

    [SerializeField]
    Fungus.Flowchart flowchart;

    public Fungus.Flowchart Flowchart { get { return flowchart; } }

    [SerializeField]
    Transform startPositon;

    public Transform StartPosition { get { return startPositon; } }

    [SerializeField]
    AreaData[] areas;

    [SerializeField]
    AreaData startingArea;

    public AreaData[] AllAreas { get { return areas; } }
    public AreaData StartingArea { get { return startingArea; } }

    [SerializeField]
    GameObject playerObject;
    public GameObject PlayerObject { get { return playerObject; } }
    PlayerActor playerActor;
    public PlayerActor PlayerActor { get { if (playerActor == null) playerActor = PlayerObject.GetComponent<PlayerActor>(); return playerActor; } }
    //[SerializeField] List<MapTeleport> teleports;

    public void Start()
    {
        GameMasterMachine.I.RegisterMission(this);
        if(areas.Length > 0 && startingArea != null)
        {
            for (int i = 0; i < areas.Length; i++)
            {
                if(areas[i]!=startingArea)
                {
                    areas[i].gameObject.SetActive(false);
                }
                else
                {
                    areas[i].gameObject.SetActive(true);
                }
            }
        }
        GameMasterMachine.I.TeleportPlayer(StartPosition.position);

        if (missionData.useGameDataStats) PlayerActor.ImportGameDataStats();
    }

    [ContextMenu("Gather and Link Areas")]
    public void GetAreas()
    {
        areas = GameObject.FindObjectsOfType<AreaData>();
        
        if(areas.Length <= 0)
        {
            Debug.LogWarning("No Areas Found");
        }
        else
        {
            List<MapTeleport> teleports = new List<MapTeleport>();
            for (int i = 0; i < areas.Length; i++)
            {
                MapTeleport[] mt = areas[i].GetTeleports();
                teleports.AddRange(mt);
            }
            //Debug.Log(teleports.Count);
            
            int maxiterations = teleports.Count * teleports.Count;
            int iterations = 0;
            while (teleports.Count >= 2)
            {

                MapTeleport a = teleports[0];
                if (iterations >= maxiterations)
                    teleports.Remove(a);
                for (int i = 1; i < teleports.Count; i++)
                {
                    MapTeleport b = teleports[i];
                    Debug.Log("Compare: " + a.name + " to " + b.name);
                    if (a.name == b.name)
                    {
                        Debug.Log("MATCH");
                        a.LinkTeleport(b);
                        teleports.Remove(a);
                        teleports.Remove(b);
                        break;
                    }
                }

                iterations++;
            }
        }
    }
}
