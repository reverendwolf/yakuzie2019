﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCActor : MonoBehaviour
{
    public enum Gesture
    {
        TalkNeutral,
        TalkNervous,
        TalkAngry,
        TalkExcited,
        CombatVictory
    }

    protected class NPCAnimationHashes
    {
        public static int doIdle = Animator.StringToHash("DoIdle");
        public static int doGesture = Animator.StringToHash("DoGesture");
        public static int breathing = Animator.StringToHash("Breathing");
        public static int inputSpeed = Animator.StringToHash("MoveInputSpeed");
        public static int mainSelect = Animator.StringToHash("MainSelector");
        public static int subSelect = Animator.StringToHash("SubSelector");
    }

    public delegate void ActorSignal();

    public ActorEventsMessenger AnimationMessenger { get { return messenger; } }
    protected ActorEventsMessenger messenger;
    protected Animator animator;
    public Animator Animator { get { return animator; } }
    protected ActorEmote emote;
    public ActorEmote Emote { get { return emote; } }
    protected NavMeshAgent agent;
    public bool IsWalking { get { return agent.hasPath; } }
    [SerializeField] protected Transform actor;
    protected Rigidbody rb;
    [SerializeField] protected ActorSettings actorSettings;
    public ActorSettings Settings { get { return actorSettings; } }

    protected Vector2 facingDirection;
    public Vector3 ForwardFacing { get { return new Vector3(facingDirection.x, 0, facingDirection.y).normalized; } }
    public Vector2 WorldPosition { get { return new Vector2(transform.position.x, transform.position.z); } }
    protected bool busy;
    public bool Busy { get { return busy; } }
    float walkAnimSpeed;
    bool turnTime = false;

    float AgentWalkSpeed { get { return agent != null ? agent.velocity.magnitude : 0; } }
    //float RealWalkSpeed { get { return Mathf.Max(AgentWalkSpeed, walkAnimSpeed); } }
    float WalkPCT { get { return AgentWalkSpeed / Settings.RunSpeed; } }
    float WalkPCTGated { get { return WalkPCT > 0.7 ? 1 : WalkPCT <= 0.1 ? 0 : 0.5f; } }

    public void DepInject(ActorEmote emote)
    {
        this.emote = emote;
    }

    protected virtual void Awake()
    {
        rb = GetComponent<Rigidbody>();
        rb.isKinematic = true;
        rb.useGravity = false;
        agent = GetComponent<NavMeshAgent>();
        if(agent != null) agent.updateRotation = false;
        if (actor != null) animator = actor.GetComponent<Animator>();
        if (messenger == null) messenger = GetComponentInChildren<ActorEventsMessenger>();
    }

    public void WalkDirection(Vector2 direction, float magnitude)
    {
        WalkDirection(direction, magnitude, false);
    }

    public void WalkDirection(Vector2 direction, float magnitude, bool sprint)
    {
        WalkDirection(direction, magnitude, Settings.WalkSpeed, sprint ? Settings.SprintSpeed : Settings.RunSpeed);
    }

    public void WalkDirection(Vector2 direction, float magnitude, float minSpeed, float maxSpeed)
    {
        if (agent == null)
            return;

        if (direction == Vector2.zero || magnitude == 0)
        {
            Idle();
            return;
        }

        direction = direction.normalized;
        if (magnitude > 0f && magnitude < 0.85f)
        {
            walkAnimSpeed = 0.5f;
        }
        else if (magnitude > 0.85f)
        {
            walkAnimSpeed = 1f;
        }
        else
        {
            walkAnimSpeed = 0f;
        }

        //animSpeed = Mathf.Clamp01(magnitude);
        //animator.SetFloat("MoveInputSpeed", Mathf.Clamp01(walkAnimSpeed));
        facingDirection = direction; 
        Vector3 velocity = new Vector3(direction.x, 0, direction.y) * (walkAnimSpeed > 0.5f ? maxSpeed : minSpeed);
        agent.velocity = velocity;
        
        TurnTowards(facingDirection);
    }

    public void WalkToPosition(Vector3 position)
    {
        if (agent == null)
            return;

        agent.SetDestination(position);
    }

    public void Idle()
    {
        walkAnimSpeed = 0;
        //animator.SetFloat("MoveInputSpeed", 0f);
        //if (cc.enabled) cc.Move(new Vector3(0, addGravity ? fall : 0, 0) * Settings.WalkSpeed * Time.deltaTime);
        if(agent != null )agent.velocity = Vector3.zero;
        TurnTowards(facingDirection);
    }

    public void FaceDirection(Vector2 direction)
    {
        if (turnTime) return;
        TurnTowards(direction);
    }

    public void TurnToFace(Vector3 position)
    {
        Vector2 dir = new Vector2(position.x, position.z) - new Vector2(transform.position.x, transform.position.z);
        facingDirection = dir;
        if (turnTime == false) StartCoroutine(TurnTime(dir));
    }

    void TurnTowards(Vector2 direction)
    {
        //Debug.DrawRay(transform.position, new Vector3(direction.x, 0, direction.y), Color.red);
        float turnAngle = Mathf.Atan2(direction.x, direction.y) * Mathf.Rad2Deg;
        Quaternion targetRotation = Quaternion.Euler(0, turnAngle, 0);
        actor.rotation = Quaternion.Slerp(actor.rotation, targetRotation, Settings.TurnSpeed * Time.deltaTime);
    }

    IEnumerator TurnTime(Vector2 direction)
    {
        float timer = Settings.TurnSpeed * Time.deltaTime;
        turnTime = true;
        while (timer > 0f)
        {
            //Debug.DrawRay(transform.position, new Vector3(direction.x, 0, direction.y));
            TurnTowards(direction);
            timer -= Time.deltaTime;
            yield return null;
        }
        turnTime = false;
    }

    public void PauseAnimation(bool pause)
    {
        SetAnimationSpeed(pause ? 0f : 1f);
    }

    public void SetAnimationSpeed(float speed)
    {
        animator.speed = speed;
    }

    public void DoGesture(Gesture gesture)
    {
        if (animator.GetBool(NPCAnimationHashes.doGesture) == true)
        {
            StartCoroutine(ChangeGesture((int)gesture + 1));
        }
        else
        {
            animator.SetInteger(NPCAnimationHashes.mainSelect, (int)gesture + 1);
            animator.SetBool(NPCAnimationHashes.doGesture, true);
        }
    }

    IEnumerator ChangeGesture(int gesture)
    {
        ReleaseGesture();
        yield return null;
        yield return null;
        animator.SetInteger(NPCAnimationHashes.mainSelect, gesture);
        animator.SetBool(NPCAnimationHashes.doGesture, true);
    }

    public void ReleaseGesture()
    {
        animator.SetBool(NPCAnimationHashes.doGesture, false);
    }

    private void Update()
    {
        if(agent!= null)
        {
            if(agent.hasPath && agent.remainingDistance <= (agent.stoppingDistance + 0.1f))
            {
                agent.ResetPath();
            }

            animator.SetFloat("MoveInputSpeed", WalkPCT);
        }
    }
}
