﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SmallTalkModule : MonoBehaviour
{
    const float AUTODELAY = 6.5f;
    [SerializeField]
    Text text;
    Transform talkerPosition;
    float offset;
    //RectTransform myRect;

    //[SerializeField]
    //int pixelBuffer = 25;
    bool activated;
    private void OnEnable()
    {
        //myRect = GetComponent<RectTransform>();
        transform.position = new Vector3(-Screen.width * 4, 0, 0);
    }

    private void Start()
    {
        transform.position = new Vector3(-Screen.width * 4, 0, 0);
        gameObject.SetActive(false);
    }

    public void Update()
    {
        if (talkerPosition != null)
        {
            transform.position = Camera.main.WorldToScreenPoint(talkerPosition.position + (Camera.main.transform.up * offset));
            /*if(!stayOnScreen)
            {
                
            }
            else
            {
                Vector3 trackPosition = Camera.main.WorldToScreenPoint(talkerPosition.position + (Camera.main.transform.up * 1.5f));
                trackPosition.x = Mathf.Clamp(trackPosition.x, 0 + (myRect.rect.width / 2f) + pixelBuffer, Screen.width - (myRect.rect.width / 2f) - pixelBuffer);
                trackPosition.y = Mathf.Clamp(trackPosition.y, 0 + (myRect.rect.height / 2f) + pixelBuffer, Screen.height - (myRect.rect.height / 2f) - pixelBuffer);
                transform.position = trackPosition;
            }*/

        }
    }

    public void ShowText(Transform position, string text, float heightOffset)
    {
        GameMasterMachine.PauseWorld += PauseCalled;
        talkerPosition = position;
        offset = heightOffset;
        UpdateText(text);
        gameObject.SetActive(true);
        //Invoke("HideText", AUTODELAY);
    }

    public void HideText()
    {
        GameMasterMachine.PauseWorld -= PauseCalled;
        talkerPosition = null;
        gameObject.SetActive(false);
    }

    public void UpdateText(string text)
    {
        this.text.text = text;
    }

    void PauseCalled(bool pause)
    {
        gameObject.SetActive(!pause);
    }
}
