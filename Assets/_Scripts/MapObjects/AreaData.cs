﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaData : MonoBehaviour
{
    [SerializeField]
    private string areaName;

    public string AreaName { get { return areaName; } }

    [SerializeField] private Transform geometry;
    [SerializeField] private Transform lighting;
    [SerializeField] private Transform exits;
    [SerializeField] private Transform props;

    public void Start()
    {
        foreach(MapTeleport mt in exits.GetComponentsInChildren<MapTeleport>())
        {
            mt.SetArea(this); //stuff
        }
    }

    public MapTeleport[] GetTeleports()
    {
        MapTeleport[] mt = exits.GetComponentsInChildren<MapTeleport>();
        foreach(MapTeleport m in mt)
        {
            m.SetArea(this);
        }
        return mt;
    }
}
