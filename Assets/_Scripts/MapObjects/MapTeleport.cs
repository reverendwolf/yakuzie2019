﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapTeleport : MapObject
{
    //[SerializeField]
    //string teleportName;
    public override MOType MapObjectType { get { return MOType.MapWarp; } }
    public override string PromptText { get { return "Enter"; } }

    [SerializeField]
    Transform exit;
    public Transform Exit { get { return exit; } }

    [SerializeField]
    MapTeleport link;
    public MapTeleport Link { get { return link; } }

    [SerializeField]
    AreaData parent;
    public AreaData ParentArea { get { return parent; } }

    public void SetArea(AreaData area)
    {
        parent = area;
    }

    public void LinkTeleport(MapTeleport linkFrom)
    {
        link = linkFrom;
        UnityEditor.EditorUtility.SetDirty(this.link);
        if (link.Link != this)
            link.LinkTeleport(this);
    }

    private void OnDrawGizmos()
    {
        if(link == null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(transform.position + Vector3.up * 1.75f, 0.5f);
            
        }
        else
        {
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(transform.position + Vector3.up, 0.5f);
            Gizmos.DrawLine(transform.position, link.transform.position);
        }

        if(exit == null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position + Vector3.up * 1.75f, 0.51f);
        }
        else
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(exit.position, 0.51f);
            Gizmos.DrawLine(transform.position, exit.position);
        }
        
    }

    private void OnValidate()
    {
        if(link != null)
        {
            link.LinkTeleport(this);
        }
    }


}
