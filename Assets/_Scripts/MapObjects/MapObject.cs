﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MapObject : MonoBehaviour
{
    public delegate void MapObjectTrigger(MapObject area);
    public static event MapObjectTrigger PlayerEntered;
    public static event MapObjectTrigger PlayerExit;

    public enum MOType { Fungus, MapWarp, RegionWarp, Pickup}
    public enum TriggerType { Action, Auto, AutoThenAction }
    public abstract MOType MapObjectType { get; }
    public abstract string PromptText { get; }

    [SerializeField]
    protected TriggerType triggerType;
    public bool StartOnTrigger { get { return (triggerType == TriggerType.Auto || triggerType == TriggerType.AutoThenAction); } }
    [SerializeField]
    bool triggerOnce;
    public bool TriggerOnce { get { return triggerOnce; } }
    protected Collider col;

    void Start()
    {
        col = GetComponent<Collider>();
        col.isTrigger = true;
    }

    public void Trigger()
    {
        if (triggerType == TriggerType.AutoThenAction)
            triggerType = TriggerType.Action;

        if (triggerOnce)
            gameObject.SetActive(false);
    }

    protected virtual void AreaTriggerEnter()
    {
        if (PlayerEntered != null) PlayerEntered(this);
    }

    protected virtual void AreaTriggerExit()
    {
        if (PlayerExit != null) PlayerExit(this);
    }

    protected void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            AreaTriggerEnter();
        }
    }

    protected void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            AreaTriggerExit();
        }
    }
}
