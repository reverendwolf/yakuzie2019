﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterActor : CombatActor
{
    [SerializeField]
    CombatMoveBase[] techniques;
    public CombatMoveBase[] Techs { get { return techniques; } }

    [SerializeField]
    MonsterPersonality personality;
    public MonsterPersonality Personality { get { return personality; } }
}
