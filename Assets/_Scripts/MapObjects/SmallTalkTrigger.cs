﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallTalkTrigger : MonoBehaviour
{
    [SerializeField]
    SmallTalkModule module;
    [TextArea][SerializeField]
    string talkText;
    [SerializeField]
    float heighOffset = 1.5f;
    

    protected void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            module.ShowText(transform, talkText, heighOffset);
        }
    }

    protected void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            module.HideText();
        }
    }

    public void UpdateText(string text)
    {
        talkText = text;
        module.UpdateText(text);
    }
}
