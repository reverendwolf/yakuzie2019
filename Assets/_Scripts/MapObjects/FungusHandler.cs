﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FungusHandler : MapObject {

    public enum InteractionVerb { Look, Talk, Pull }

    public override MOType MapObjectType { get { return MOType.Fungus; } }
    public override string PromptText { get { return new string[] { "Look", "Talk", "Pull" }[(int)verb]; } }

    [SerializeField]
    string flowchartLink;
    public string FlowchartLink { get { return flowchartLink; } }

    [SerializeField]
    InteractionVerb verb;

    public void UpdateFlowchartLink(string newLink)
    {
        flowchartLink = newLink;
    }
}
