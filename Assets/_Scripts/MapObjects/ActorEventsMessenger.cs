﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActorEventsMessenger : MonoBehaviour {

    public delegate void AnimEventHit(CombatActor.HitReaction reaction = CombatActor.HitReaction.Mid);
    public delegate void AnimEvent();
    public event AnimEventHit OnHit;
    public event AnimEvent AttackFinished;

    public void HitGeneric()
    {
        if (OnHit != null) OnHit(CombatActor.HitReaction.Mid);
    }

    public void HitHigh()
    {
        if (OnHit != null) OnHit(CombatActor.HitReaction.High);
    }

    public void HitMid()
    {
        if (OnHit != null) OnHit(CombatActor.HitReaction.Mid);
    }

    public void HitLow()
    {
        if (OnHit != null) OnHit(CombatActor.HitReaction.Low);
    }

    public void HitFinished()
    {
        if (AttackFinished != null) AttackFinished();
    }

    public void FootL()
    {

    }

    public void FootR()
    {

    }

    public void Jump()
    {

    }

    public void Land()
    {

    }
}
