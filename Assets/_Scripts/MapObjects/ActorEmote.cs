﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActorEmote : MonoBehaviour
{
    public const float EMOTEWAIT = 2f;
    const float EMOTEMINWAIT = 0.75f;
    float popTime;
    SpriteRenderer sr;
    Transform c;
    public enum Emote
    {
        Question,
        Exclamation,
        DoubleExclamation,
        Crestfallen,
        Teardrop,
        DoubleTeardrop,
        Cloud,
        Heart,
        DoubleHeart,
        Hearbreak,
        Lightbulb,
        Anger,
        Surprise,
        Star,
        Spiral,
        Music,
        BigEx,
        BigO,
        Haha
    }
    [SerializeField]
    Sprite[] emotes;
    Coroutine emoting;
    float RandomDelay { get { return Random.Range(0.01f, 0.2f); } }
    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponentInChildren<SpriteRenderer>();
        sr.enabled = false;
        c = Camera.main.transform;
        if (transform.parent.GetComponent<NPCActor>()) transform.parent.GetComponent<NPCActor>().DepInject(this);
    }

    public void PopEmote(Emote emote)
    {
        //Debug.Log("EMOTE POP: " + this.transform.parent.name);
        if (emoting != null)
        {
            StopCoroutine(emoting);
            emoting = null;
            popTime = 0f;
        }

        emoting = StartCoroutine(DoPopEmote(emote));
    }

    public void KillEmote()
    {
        if (emoting != null)
        {
            StopCoroutine(emoting);
            emoting = null;
        }
        emoting = StartCoroutine(DoKillEmote(3));
    }

    public void ShowEmote(Emote emote)
    {
        sr.sprite = emotes[(int)emote];
        transform.rotation = c.transform.rotation;
        sr.enabled = true;
        sr.color = Color.white;
    }

    IEnumerator DoPopEmote(Emote emote)
    {
        yield return new WaitForSecondsRealtime(RandomDelay);
        ShowEmote(emote);
        yield return new WaitForSecondsRealtime(EMOTEWAIT + RandomDelay);
        yield return StartCoroutine(DoKillEmote());
    }

    IEnumerator DoKillEmote(float killSpeed = 1)
    {
        while(popTime <= EMOTEMINWAIT)
        {
            yield return null;
        }
        while (sr.color.a >= 0f)
        {
            sr.color = new Color(1, 1, 1, sr.color.a - (Time.deltaTime * killSpeed));
            yield return null;
        }
        sr.color = new Color(1, 1, 1, 0);
        sr.enabled = false;
        emoting = null;
        popTime = 0f;
    }

    private void Update()
    {
        if (sr.enabled)
        {
            //transform.LookAt(c);
            transform.rotation = c.transform.rotation;
            popTime += Time.unscaledDeltaTime;
        }

        if (Input.GetKey(KeyCode.Alpha1))
        {
            PopEmote(Emote.Exclamation);
        }

        if (Input.GetKey(KeyCode.Slash))
        {
            PopEmote((Emote)Random.Range(0,19));
        }

        if (Input.GetKey(KeyCode.Alpha3))
        {
            KillEmote();
        }
    }
}
