﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActor : CombatActor
{
    public struct SaveDataPackage
    {
        public int characterID;
        public MonsterStatSheet statSheet;

        public SaveDataPackage(int characterID, MonsterStatSheet statSheet)
        {
            this.characterID = characterID;
            this.statSheet = statSheet;
        }
    }

    [SerializeField]
    GameData.Character character;
    public int CharacterID { get { return (int)character - 1; } }
    public SaveDataPackage SaveData { get { return new SaveDataPackage(CharacterID, statSheet); } }
    public PlayerTemplate PlayerTemplate { get { return (PlayerTemplate)combatTemplate; } }
    [SerializeField]
    MonsterDeathblowUpgrade[] deathBlows = new MonsterDeathblowUpgrade[8];
    public MonsterDeathblowUpgrade[] Deathblows { get { return deathBlows; } }

    [SerializeField]
    MonsterMoveUpgrade[] techniques = new MonsterMoveUpgrade[10];
    public MonsterMoveUpgrade[] Techs { get { return techniques; } }

    public void ImportGameDataStats()
    {
        statSheet = GameMasterMachine.I.GameData.GetStats(character);
    }

    public CombatMoveBase GetDeathblowInSlot(MonsterDeathblowUpgrade.DeathblowList slot)
    {
        for (int i = 0; i < deathBlows.Length; i++)
        {
            if(statSheet.dbUnlocked[i] && deathBlows[i].Slot == slot)
            {
                return deathBlows[i].Move;
            }
        }

        return null;
    }

    public bool HasDeathblowInSlot(MonsterDeathblowUpgrade.DeathblowList slot)
    {
        for (int i = 0; i < deathBlows.Length; i++)
        {
            if (statSheet.dbUnlocked[i] && deathBlows[i].Slot == slot)
            {
                return true;
            }
        }

        return false;
    }

    public CombatMoveBase[] GetAllUnlockedTechs()
    {
        List<CombatMoveBase> _return = new List<CombatMoveBase>();

        for (int i = 0; i < techniques.Length; i++)
        {
            if(statSheet.techUnlocked[i])
            {
                _return.Add(techniques[i].Move);
            }
        }

        return _return.ToArray();
    }
}
