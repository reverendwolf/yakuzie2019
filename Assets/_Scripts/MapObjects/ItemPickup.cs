﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickup : MapObject
{
    [SerializeField]
    CombatMoveBase item;

    int itemIndex;
    public int ItemIndex { get { return itemIndex; } }

    public override MOType MapObjectType { get { return MOType.Pickup; } }

    public override string PromptText { get { return "Pickup"; } }

    public void Start()
    {
        if(GameMasterMachine.I != null)
            itemIndex = GameMasterMachine.I.Settings.GetItemIndex(item);

        if (itemIndex < 0)
        {
            Debug.LogWarning("Improperly registered item!", this);
            this.gameObject.SetActive(false);
        }
            
    }
}
