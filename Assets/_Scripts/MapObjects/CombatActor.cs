﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Rigidbody))]
public abstract class CombatActor : NPCActor
{
    public static int SPBARPOINTS = 32;

    public enum HitReaction
    {
        Low,
        Mid,
        High,
        Dodge,
        Parry,
        Buff,
        Heal
    }
    public event ActorSignal OnBattleUpdate = delegate { };
    public event ActorSignal OnBattleCharge = delegate { };
    Vector3 defaultPosition;

    [SerializeField] Transform heart;

    [SerializeField]
    protected MonsterTemplate combatTemplate;
    public MonsterTemplate Template { get { return combatTemplate; } }
    [SerializeField]
    MoveWeapon combatWeapon;
    public MoveWeapon Weapon { get { return combatWeapon; } }
    [SerializeField]
    CombatMoveBase overdrive;
    public CombatMoveBase Overdrive { get { return overdrive; } }
    [SerializeField]
    protected MonsterStatSheet statSheet;
    public int CurHP { get { return statSheet.curHP; } set { statSheet.curHP = Mathf.Clamp(value, 0, MaxHP); } }
    public float CurHPPercent { get { return ((float)statSheet.curHP / (float)MaxHP); } }
    public int CurSP { get { return statSheet.curSP; } set { statSheet.curSP = Mathf.Clamp(value, 0, MaxSP); } }
    //public float CurSPPercent { get { return (float)CurSP / (float)(SPBARPOINTS * MaxAP); } }
    public float CurSPPercent { get { return (statSheet.curSP % SPBARPOINTS) / (float)SPBARPOINTS; } }
    public int CurSPBars { get { return Mathf.FloorToInt((float)statSheet.curSP / (float)SPBARPOINTS); } }
    public int MaxHP { get { return combatTemplate.BasicInfo.StartingHP + (combatTemplate.BasicInfo.BonusHP * statSheet.hp); } }
    public int MaxAP { get { return Mathf.Clamp(statSheet.ap, 1, 6); } }
    public int MaxSP { get { return (MaxSPBars * SPBARPOINTS); } }
    public int MaxSPBars { get { return (2 + statSheet.ap); } }
    [SerializeField]
    MonsterBattleData battleData;
    public MonsterBattleData BattleData { get { return battleData; } set { battleData = value; } }
    public Vector3 HeartPosition { get { if (heart != null) return heart.position; else return transform.position; } } 
    bool directionalInput;

    public void OnValidate()
    {
        statSheet.curHP = Mathf.Clamp(statSheet.curHP, 0, MaxHP);
    }

    

    public int GetVirtueValue(MonsterStatSheet.Virtues virtue)
    {
        switch (virtue)
        {
            case MonsterStatSheet.Virtues.HP:
                return statSheet.hp;
            case MonsterStatSheet.Virtues.AP:
                return statSheet.ap;
            case MonsterStatSheet.Virtues.ATK:
                return statSheet.atk;
            case MonsterStatSheet.Virtues.EXP:
                return statSheet.expBoost ? 1 : 0;
            case MonsterStatSheet.Virtues.GP:
                return statSheet.gpBoost ? 1 : 0;
            default:
                return 0;
        }
    }

    public void PaySPBar(float bars)
    {
        CurSP -= Mathf.CeilToInt(bars * SPBARPOINTS);
    }

    protected class CombatAnimationHashes : NPCAnimationHashes
    {
        public static int doCombat = Animator.StringToHash("DoCombat");
        public static int hitSelect = Animator.StringToHash("HitSelector");
        public static int onFire = Animator.StringToHash("OnFire");
        public static int onCrush = Animator.StringToHash("OnCrush");
        public static int onDie = Animator.StringToHash("OnDie");
        public static int onHit = Animator.StringToHash("OnHit");
        public static int inCombat = Animator.StringToHash("InCombat");
    }

    public bool IsWeak(MoveDamageType type)
    {
        return battleData.IsWeak(type) || Template.Weak.Contains(type);
    }

    public bool IsResist(MoveDamageType type)
    {
        return battleData.IsResist(type) || Template.Weak.Contains(type);
    }

    public bool IsImmune(MoveDamageType type)
    {
        return battleData.IsImmune(type) || Template.Weak.Contains(type);
    }

    [ContextMenu("Reset Vitals")]
    void ResetVitals()
    {
        CurHP = MaxHP;
        CurSP = SPBARPOINTS;
    }

    public void SetAgentPriority(int priority)
    {
        agent.avoidancePriority = priority;
    }

    public void CombatWalkDirection(Vector2 direction, float magnitude)
    {
        WalkDirection(direction, magnitude, Settings.WalkSpeed, Settings.CombatRunSpeed);
    }

    public void Teleport(Vector3 position)
    {
        if(!agent.Warp(position))
        {
            Debug.LogError("Warp Failed!");
        }
    }

    public IEnumerator DoCombatAction(VSCombatMachine.ActionItem attackAction)
    {
        animator.SetInteger(CombatAnimationHashes.mainSelect, attackAction.actionType == VSCombatMachine.ActionItem.ActionType.Melee ? 1 : 2);
        animator.SetInteger(CombatAnimationHashes.subSelect, attackAction.actionAnimation);
        animator.SetBool(CombatAnimationHashes.doCombat, true);
        yield return null;
        while (animator.GetBool(CombatAnimationHashes.doCombat))
        {
            yield return null;
        }
    }

    public void HitReact()
    {
        animator.SetInteger(CombatAnimationHashes.mainSelect, 3);
        animator.SetInteger(CombatAnimationHashes.subSelect, Random.Range(1,4));
        animator.SetBool(CombatAnimationHashes.doCombat, true);
    }

    public void HitReact(HitReaction reaction)
    {
        animator.SetInteger(CombatAnimationHashes.hitSelect, (int)reaction + 1);
        animator.SetTrigger(CombatAnimationHashes.onHit);
    }

    public void Die(bool die)
    {
        animator.SetBool(CombatAnimationHashes.onDie, die);
        animator.SetFloat(CombatAnimationHashes.breathing, die ? 0f : 1.0f);
        this.enabled = !die;
        rb.detectCollisions = !die;
        agent.enabled = !die;
        battleData.BattleStance = MonsterBattleData.Stance.KO;
    }

    public void EnterCombat(bool enterCombat)
    {
        if (enterCombat)
        {
            battleData = new MonsterBattleData();
            animator.SetBool(CombatAnimationHashes.inCombat, true);
            ReleaseGesture();
        }
        else
        {
            animator.SetBool(CombatAnimationHashes.inCombat, false);
        }
    }

    public void BattleUpdate()
    {
        OnBattleUpdate();
    }

}

[System.Serializable]
public class ActorSettings
{
    [SerializeField] float walkSpeed = 2;
    public float WalkSpeed { get { return walkSpeed; } }
    [SerializeField] float runSpeed = 4;
    public float RunSpeed { get { return runSpeed; } }
    [SerializeField] float combatRunSpeed = 3;
    public float CombatRunSpeed { get { return combatRunSpeed; } }
    [SerializeField] float sprintSpeed = 5;
    public float SprintSpeed { get { return sprintSpeed; } }
    [SerializeField] float turnSpeed = 30;
    public float TurnSpeed { get { return turnSpeed; } }
}