﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtensionMethods
{

    public static Vector2 RelativeDirection(this Camera cam, Vector2 direction)
    {
        if (direction == Vector2.zero) return direction;
        float facing = cam.transform.rotation.eulerAngles.y;
        Vector3 turnedInput = Quaternion.Euler(0, facing, 0) * new Vector3(direction.x, 0, direction.y);
        return new Vector2(turnedInput.x, turnedInput.z);
    }

    public static Vector2 ConvertToV2(this Vector3 v3)
    {
        return new Vector2(v3.x, v3.z);
    }

    public static Vector3 Flatten(this Vector3 v3)
    {
        return new Vector3(v3.x, 0, v3.z);
    }
}
