﻿using InControl;
using UnityEngine;

public class InputMaster : MonoBehaviour
{
    const int SMOOTHING_FRAMES = 5;
    public enum ButtonInput { Up, Down, Left, Right, Square, Triangle, Circle, Cross, L1, R1}
    public enum ButtonModifier { None, L1, R1}

    public delegate void ActionButtonSingle(ButtonInput button, InputMaster.ButtonModifier modifier = ButtonModifier.None);
    public delegate void ActionButtonHold(ButtonInput button, float holdTime, InputMaster.ButtonModifier modifier = ButtonModifier.None);
    public delegate void StickInput(Vector2 direction);

    public static event ActionButtonSingle OnActionPressed;
    public static event ActionButtonHold OnActionHeld;
    public static event ActionButtonSingle OnActionReleased;
    public static event StickInput LeftStickDirection;
    public static event StickInput LeftStickRelativeDirection;
    public static event StickInput RightStickDirection;

    public const float THRESHOLD = 1.2f;
    public const float DEADZONE = 0.1f;

    public ButtonInput actionHeld;
    public float holdTime;
    public int holdFrames;
    public bool holding;
    public ButtonModifier actionModifier;

    public Vector2[] leftStickInput;
    public Vector2 LeftStickAverage { get { return (leftStickInput[0] + leftStickInput[1] + leftStickInput[2]) / 3f; } }
    public Vector2 rightStickInput;

    int stickInputFrame;

    private void Start()
    {
        leftStickInput = new Vector2[SMOOTHING_FRAMES];
    }

    public bool ButtonPressed(InputMaster.ButtonInput button)
    {
        //Debug.Log("H: " + holding + " F: " + (holdFrames == 1) + " B: " + (actionHeld == button));
        return holding && holdFrames == 1 && actionHeld == button;
    }


    // Update is called once per frame
    void Update()
    {
        InputDevice ad = InputManager.ActiveDevice;

        if(ad.LeftBumper.IsPressed && !ad.RightBumper.IsPressed)
        {
            actionModifier = ButtonModifier.L1;
        }
        else if(ad.RightBumper.IsPressed && !ad.LeftBumper.IsPressed)
        {
            actionModifier = ButtonModifier.R1;
        }
        else
        {
            actionModifier = ButtonModifier.None;
        }

        HoldingButton(KeyCode.UpArrow, ad.DPadUp, ButtonInput.Up);
        HoldingButton(KeyCode.DownArrow, ad.DPadDown, ButtonInput.Down);
        HoldingButton(KeyCode.LeftArrow, ad.DPadLeft, ButtonInput.Left);
        HoldingButton(KeyCode.RightArrow, ad.DPadRight, ButtonInput.Right);

        HoldingButton(KeyCode.J, ad.Action3, ButtonInput.Square);
        HoldingButton(KeyCode.I, ad.Action4, ButtonInput.Triangle);
        HoldingButton(KeyCode.K, ad.Action1, ButtonInput.Cross);
        HoldingButton(KeyCode.L, ad.Action2, ButtonInput.Circle);

        HoldingButton(KeyCode.U, ad.LeftBumper, ButtonInput.L1);
        HoldingButton(KeyCode.O, ad.RightBumper, ButtonInput.R1);

        //HoldingDirections(KeyCode.A, ad.LeftBumper, ButtonInput.L1);
        //HoldingDirections(KeyCode.D, ad.RightBumper, ButtonInput.R1);

        //Vector2 fakeLeftStick = new Vector2((Input.GetKey(KeyCode.D) ? 1 : Input.GetKey(KeyCode.A) ? -1 : 0), (Input.GetKey(KeyCode.W) ? 1 : Input.GetKey(KeyCode.S) ? -1 : 0));

        //bool leftStickInput = (fakeLeftStick == Vector2.zero) || (ad.LeftStick.LastValue != Vector2.zero);
        float inputVal = Input.GetKey(KeyCode.LeftShift) ? 0.15f : 1f;
        float kbh = Input.GetKey(KeyCode.A) ? -inputVal : Input.GetKey(KeyCode.D) ? inputVal : 0;
        float kbv = Input.GetKey(KeyCode.S) ? -inputVal : Input.GetKey(KeyCode.W) ? inputVal : 0;
        Vector2 kbInput = new Vector2(kbh, kbv);
        if(kbInput == Vector2.zero)
        {
            leftStickInput[stickInputFrame] = ad.LeftStick.LastValue;
        }
        else
        {
            leftStickInput[stickInputFrame] = kbInput;
        }
        //leftStickInput[stickInputFrame] = ad.LeftStick.LastValue;
        Vector2 leftSmooth = Vector2.zero;
        for (int i = 0; i < leftStickInput.Length; i++)
        {
            leftSmooth += leftStickInput[i];
        }
        leftSmooth /= leftStickInput.Length;

        if (LeftStickDirection != null)
        {
            if (Mathf.Abs(leftSmooth.x) < DEADZONE) leftSmooth.x = 0;
            if (Mathf.Abs(leftSmooth.y) < DEADZONE) leftSmooth.y = 0;
            LeftStickDirection(leftSmooth);
        }
            

        if (LeftStickRelativeDirection != null) LeftStickRelativeDirection(Camera.main.RelativeDirection(leftSmooth));

        stickInputFrame++;
        stickInputFrame = stickInputFrame % SMOOTHING_FRAMES;
        rightStickInput = ad.RightStick.LastValue;
        if(RightStickDirection != null)
        {
            Vector2 rStickFiltered = ad.RightStick.LastValue;
            if (Mathf.Abs(rStickFiltered.x) < DEADZONE) rStickFiltered.x = 0;
            if (Mathf.Abs(rStickFiltered.y) < DEADZONE) rStickFiltered.y = 0;

            RightStickDirection(rStickFiltered);
        }
    }

    void HoldingButton(KeyCode key, InControl.InputControl control, ButtonInput button)
    {
        if (Input.GetKeyDown(key) || control.WasPressed)
        {
            actionHeld = button;
            holding = true;
            holdTime = 0f;
            holdFrames = 1;
            if (OnActionPressed != null)
            {
                OnActionPressed(actionHeld);
            }
            //Debug.Log("Pressed " + button.ToString());
        }
        else if (Input.GetKey(key) || control.IsPressed)
        {
            if (actionHeld == button && holding == true)
            {
                holdTime += Time.deltaTime;
                holdFrames++;

                if (OnActionHeld != null)
                {
                    OnActionHeld(actionHeld, holdTime);
                }
            }
        }
        else if (Input.GetKeyUp(key) || control.WasReleased)
        {
            if (actionHeld == button && holding == true)
            {
                holdTime = 0f;
                holding = false;
                holdFrames = 0;
                
                //Debug.Log("Released " + button.ToString());
            }

            if (OnActionReleased != null)
            {
                OnActionReleased(button);
            }
        }
    }
}

public class PlayerInputActions : PlayerActionSet
{
    public PlayerAction UpArrow;
    public PlayerAction DownArrow;
    public PlayerAction LeftArrow;
    public PlayerAction RightArrow;

    public PlayerAction UpAction;
    public PlayerAction DownAction;
    public PlayerAction LeftAction;
    public PlayerAction RightAction;

    public PlayerAction LeftBumper;
    public PlayerAction RightBumper;

    public PlayerAction Pause;

    public PlayerInputActions()
    {
        UpArrow = CreatePlayerAction("Up Arrow");
        DownArrow = CreatePlayerAction("Down Arrow");
        LeftArrow = CreatePlayerAction("Left Arrow");
        RightArrow = CreatePlayerAction("Right Arrow");
    }
}