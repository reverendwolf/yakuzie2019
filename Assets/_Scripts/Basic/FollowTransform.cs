﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTransform : MonoBehaviour {

    Transform follow;

    public void Follow(Transform follow) { this.follow = follow; }

    private void Update()
    {
        if (follow)
            transform.position = follow.position;
    }
}
