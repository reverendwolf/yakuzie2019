﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class FreelookCameraInput : MonoBehaviour {

    CinemachineFreeLook fcCam;
    CinemachineVirtualCamera vCam;
    CinemachineOrbitalTransposer vCamOrbit;

    private void OnEnable()
    {
        vCam = GetComponent<CinemachineVirtualCamera>();
        if (vCam != null) vCamOrbit = vCam.GetCinemachineComponent<CinemachineOrbitalTransposer>();
        fcCam = GetComponent<CinemachineFreeLook>();
        InputMaster.RightStickDirection += StickInput;
    }

    private void OnDisable()
    {
        InputMaster.RightStickDirection -= StickInput;
    }

    void StickInput(Vector2 input)
    {
        if(fcCam != null)
        {
            fcCam.m_XAxis.m_InputAxisValue = input.x;
            fcCam.m_YAxis.m_InputAxisValue = input.y;
        }
        
        if(vCam != null)
        {
            vCamOrbit.m_XAxis.m_InputAxisValue = input.x;
        }
    }
}
