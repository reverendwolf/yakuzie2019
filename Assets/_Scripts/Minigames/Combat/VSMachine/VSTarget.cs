﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VSTarget : MonoBehaviour
{
    public struct TargetData
    {
        CombatActor actor;
        int hpLimit;
    }

    CombatActor actorReference;
    CombatActor Actor { get { return actorReference;                       } }

    int remainingHP;

    public void Initialize(CombatActor actorReference)
    {
        this.actorReference = actorReference;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
