﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class VSCombatAI
{
    private delegate void AIBehavior();
    private AIBehavior CurrentBehavior;

    public enum MoveTypeSelected
    {
        None,
        Combo,
        Ability
    }

    public enum ChaseMode
    {
        Idle, Player, Target
    }

    MonsterActor actor;
    VSCombatStage stage;
    //VSCombatMachine machine;
    public MonsterActor Actor { get { return actor; } }
    PlayerActor PlayerActor { get { return stage.PlayerActor; } }

    MonsterPersonality Personality { get { return actor.Personality; } }

    Vector3 PlayerPosition { get { return PlayerActor.transform.position; } }
    Vector3 ActorPosition { get { return Actor.transform.position; } }

    Vector3 PlayerDirection { get { return PlayerPosition - ActorPosition; } }
    float PlayerDistance { get { return PlayerDirection.sqrMagnitude; } }

    Vector3 PreferredPosition { get { return (-PlayerDirection.normalized * Personality.OptimalRange) + PlayerPosition; } }
    Vector3 PreferredDirection { get { return ActorPosition - PreferredPosition; } }
    //float PreferredDistance { get { return Data.SafeRange * Data.SafeRange; } }

    Vector3 SafeDirection { get { return stage.SafePoint() - Actor.transform.position; } }
    float SafeDistance { get { return SafeDirection.sqrMagnitude; } }

    Vector3 currentDestination;
    Vector3 DestinationDirection { get { return ActorPosition - currentDestination; } }
    float DestinationDistance {  get { return DestinationDirection.sqrMagnitude; } }

    bool CTHigh { get { return actor.BattleData.CTFull; } }

    int selectedAbility;
    int selectedTarget; //0 = player, 1+ = enemyIndex?

    MoveTypeSelected selectedType;
    public MoveTypeSelected SelectedType { get { return selectedType; } }
    ChaseMode chaseMode;

    //float arbitraryWait = 0;
    //float updateTarget = 0;

    public CombatMoveBase SelectedAbility { get { return actor.Techs[selectedAbility]; } }
    public MoveWeapon MeleeWeapon { get { return actor.Weapon; } }
    public int SelectedAbilityIndex { get { return selectedAbility; } }
    public int SelectedTargetIndex { get { return selectedTarget; } }

    bool AwayFromPlayerClear { get { Debug.DrawRay(Actor.transform.position + Vector3.up, -PlayerDirection, Color.red, 0.15f); return !Physics.Raycast(Actor.transform.position + Vector3.up, -PlayerDirection, 2f); } }
    bool SafeSpaceClear { get { Debug.DrawRay(Actor.transform.position + Vector3.up, SafeDirection, Color.yellow, 0.15f); return !Physics.Raycast(Actor.transform.position + Vector3.up, SafeDirection, 2f); } }

    bool MoveSelected { get { return selectedType != MoveTypeSelected.None; } }
    bool InRange { get { return MoveSelected ? (PlayerDistance < (selectedType == MoveTypeSelected.Ability ? (SelectedAbility.Targetting.Range * SelectedAbility.Targetting.Range) * 0.75f : (MeleeWeapon.Targetting.Range * MeleeWeapon.Targetting.Range) * 0.75f)) : false; } }

    public bool AttackReady { get { return MoveSelected && InRange; } }

    float DamageTaken { get { return (float)(actor.MaxHP - actor.CurHP) / (float)actor.MaxHP; } }

    bool CanWalkDirection (Vector3 direction)
    {
        Debug.DrawRay(Actor.transform.position + Vector3.up, direction, Color.blue, 0.15f);
        return !Physics.Raycast(Actor.transform.position + Vector3.up, direction, 2f);
    }

    float wave1;
    float wave2;
    //float wave3;
    float waveOffset;

    float idleTime;

    TextMesh tm;
    public VSCombatAI(ref MonsterActor actor, VSCombatStage stage, VSCombatMachine machine)
    {
        this.actor = actor;
        this.stage = stage;
        //this.machine = machine;
        Actor.TurnToFace(PlayerActor.transform.position);
        tm = Actor.GetComponentInChildren<TextMesh>();
        waveOffset = Random.value;
        //PickDestination();
        //Debug.Log("AI Created for actor: " + this.actor, this.actor);
    }

    void Attack()
    {
        //((VSCBattleRun)machine.CurrentState).AIAttack(card, machine.PlayerCard, Data.MeleeWeapon);
    }

    public void ForceThink()
    {
        selectedType = MoveTypeSelected.None;
        if(CurrentBehavior != null)
        {
            CurrentBehavior = null;
        }
    }

    public void Tick()
    {
        Think();

        if(CurrentBehavior == null)
        {
            //Debug.Log("No Behavior");
            if(PlayerDistance < 1f)
            {
                waitTime = Random.Range(1.5f, 3.5f);
                CurrentBehavior += WaitForAWhile;
            }
            else
            {
                CurrentBehavior += GoToPlayer;
            }
            //CurrentBehavior += WaitForAWhile;
        }
        else
        {
            CurrentBehavior();
        }
    }

    void GoToPlayer()
    {
        if (tm) tm.text = "Walking forwards. " + PlayerDistance.ToString("0.0#");

        Actor.CombatWalkDirection(PlayerDirection.ConvertToV2(), CTHigh ? 1f : 0.35f);

        if (PlayerDistance < 0.8f)
        {
            CurrentBehavior -= GoToPlayer;
            if (CTHigh && !MoveSelected)
                SelectMove();

            if(MoveSelected)
            {
                //Attack();
                //CurrentBehavior = null;
            }
        }
            

        
    }

    float waitTime;

    void WaitForAWhile()
    {
        if (tm) tm.text = "Waiting for a while... " + waitTime.ToString("0.0#");

        Actor.Idle();
        Actor.TurnToFace(PlayerPosition);
        waitTime -= Time.deltaTime;

        if (waitTime <= 0f)
            CurrentBehavior -= WaitForAWhile;
    }


    void SelectMove()
    {
        /*
        if(PlayerDistance < PreferredDistance)
        {
            //close range
            selectedType = MoveTypeSelected.Combo;
            selectedTarget = 0;
            Debug.Log(card.cardName + " selected Melee");
        }
        else if(Data.Abilities.Length > 0)
        {
            //long range
            selectedType = MoveTypeSelected.Ability;
            selectedAbility = Random.Range(0, Data.Abilities.Length);
            selectedTarget = 0;
            Debug.Log(card.cardName + " selected Ability");
        }
        else
        {
            selectedType = MoveTypeSelected.None;
        }*/

        selectedType = MoveTypeSelected.Combo;
        selectedTarget = 0;
        //Debug.Log(actor.CharacterData.Template.BasicInfo.DisplayName + " selected Melee");

    }

    void Think()
    {
        float time = Time.time * 0.075f;
        wave1 = Mathf.PerlinNoise(waveOffset, time);
        wave2 = Mathf.PerlinNoise(waveOffset + 1.5f, time);
    }
}
