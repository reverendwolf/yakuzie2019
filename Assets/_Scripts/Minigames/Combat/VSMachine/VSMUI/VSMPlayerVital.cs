﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VSMPlayerVital : MonoBehaviour {

    [SerializeField]
    Image ctFill;
    [SerializeField]
    Image hpFill;
    [SerializeField]
    Text hpText;
    [SerializeField]
    Image spFill;
    [SerializeField]
    Text spText;

    VSMUI ui;

    Coroutine hp;
    Coroutine sp;

    [SerializeField]
    float barSpeed = 2.0f;

    public void Inject(VSMUI ui)
    {
        this.ui = ui;
        //UpdateCT();
        ui.PlayerActor.OnBattleUpdate += UpdateVisuals;
        ForceUpdateAll();
    }

    void ForceUpdateAll()
    {
        if(CanUpdate)
        {
            ctFill.fillAmount = ui.PlayerActor.BattleData.CT;
            hpText.text = ui.PlayerActor.CurHP + "/" + ui.PlayerActor.MaxHP;
            spText.text = ui.PlayerActor.CurSPBars.ToString();
            hpFill.fillAmount = ui.PlayerActor.CurHPPercent;
            spFill.fillAmount = ui.PlayerActor.CurSPPercent;
        }
    }

    bool CanUpdate { get { return ui != null && ui.PlayerActor != null; } }

    public void UpdateCT()
    {
        if(CanUpdate) ctFill.fillAmount = ui.PlayerActor.BattleData.CT;
    }

    void UpdateVisuals()
    {
        UpdateHP();
        UpdateSP();
    }

    void UpdateHP()
    {
        if (CanUpdate)
        {
            hpText.text = ui.PlayerActor.CurHP + "/" + ui.PlayerActor.MaxHP;
            if (hp != null) StopCoroutine(hp);
            hp = StartCoroutine(MoveFillPercent(hpFill, ui.PlayerActor.CurHPPercent));
        }
    }

    IEnumerator MoveFillPercent(Image fillImage, float targetPercent)
    {
        while(fillImage.fillAmount != targetPercent)
        {
            yield return null;
            fillImage.fillAmount = Mathf.MoveTowards(fillImage.fillAmount, targetPercent, Time.deltaTime * barSpeed);
        }
    }

    void UpdateSP()
    {
        if(CanUpdate)
        {
            spText.text = ui.PlayerActor.CurSPBars.ToString();
            if (sp != null) StopCoroutine(sp);
            sp = StartCoroutine(MoveFillPercent(spFill, ui.PlayerActor.CurSPPercent));
        }
    }

    public void Update()
    {
        UpdateCT();
    }

    public void OnDisable()
    {
        ui.PlayerActor.OnBattleUpdate -= UpdateVisuals;
    }
}
