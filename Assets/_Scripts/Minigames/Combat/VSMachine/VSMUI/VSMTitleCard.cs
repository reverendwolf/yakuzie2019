﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VSMTitleCard : MonoBehaviour
{
    Animation anim;
    [SerializeField]
    Text titleText;

    public bool IsPlaying { get { return anim.isPlaying; } }
    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animation>();
    }

    public void ShowTitle(string title)
    {
        titleText.text = title;
        anim.Play();
    }
}
