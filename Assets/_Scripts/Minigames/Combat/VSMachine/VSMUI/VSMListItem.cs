﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
public class VSMListItem : MonoBehaviour
{
    [SerializeField]
    Image iconA;
    [SerializeField]
    Text label;
    [SerializeField]
    Image iconB;
    [SerializeField]
    Image cost;

    CanvasGroup cg;

    public bool IsEmtpy { get { return cg.alpha == 0f; } }

    private void Start()
    {
        cg = GetComponent<CanvasGroup>();
    }

    public void Show(string label)
    {
        this.label.text = label;
        if(cg == null) cg = GetComponent<CanvasGroup>();
        cg.alpha = 1;
    }

    public void ShowNothing()
    {
        if (cg == null) cg = GetComponent<CanvasGroup>();
        cg.alpha = 0;
    }
}
