﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VSMUI : MonoBehaviour {

    [SerializeField]
    VSMPlayerVital playerVital;
    //[SerializeField]
    //VSMPlayerControls playerControls;
    [SerializeField]
    VSMComboModule playerCombo;
    [SerializeField]
    VSMTargetReticle targetReticle;
    [SerializeField]
    VSMList abilityList;
    [SerializeField]
    VSMTitleCard titleCard;
    [SerializeField]
    VSMTitleCard moveCard;

    public VSMList AbilityList { get { return abilityList; } }

    [SerializeField]
    GameObject numPopperObject;
    List<NumberPopper> numberPoppers;

    CombatActor playerActor;
    //public CombatCard PlayerCard { get { return playerCard; } }
    public CombatActor PlayerActor { get { return playerActor; } }

    public bool ShowingTitle { get { return titleCard.IsPlaying; } }

    public void Start()
    {
        numberPoppers = new List<NumberPopper>();
    }

    public void Initialize(ref CombatActor playerActor)
    {
        this.playerActor = playerActor;
        playerVital.Inject(this);
        gameObject.SetActive(true);
    }

    public void ShowTitleCard(string title)
    {
        titleCard.ShowTitle(title);
    }

    public void ShowMoveCard(string moveName)
    {
        moveCard.ShowTitle(moveName);
    }



    NumberPopper GetNextPopper()
    {
        for (int i = 0; i < numberPoppers.Count; i++)
        {
            if (numberPoppers[i].Ready)
            {
                return numberPoppers[i];
            }
        }
        numberPoppers.Add(Instantiate(numPopperObject, this.transform).GetComponent<NumberPopper>());
        return numberPoppers[numberPoppers.Count - 1];
    }

    public void PopMessage(CombatActor actor, string message, Color color, bool scramble)
    {
        Debug.Log("Popping on " + actor.name);
        NumberPopper next = GetNextPopper();
        if (scramble) next.ScramblePop(message, color, actor.transform);
        else next.ShowPop(message, color, actor.transform);
    }

    public void SetControls(VSMPlayerControls.ControlsSet set)
    {
        GameMasterMachine.I.Controls.SetContols(set);
    }

    public void SetControls(MoveWeapon weapon)
    {
        GameMasterMachine.I.Controls.SetWeaponControls(weapon);
    }

    public void ShowCombo()
    {
        playerCombo.StartCombo(playerActor.MaxAP);
    }

    public void AddComboAttack(int attack)
    {
        playerCombo.AddAttack(attack);
    }

    public void ShowSpecial(string specialName)
    {
        playerCombo.ShowSpecial(specialName);
    }

    public void HideSpecial()
    {
        playerCombo.HideSpecial();
    }

    public void HideCombo()
    {
        playerCombo.HideCombo();
    }

    public void StartComboDamage()
    {
        playerCombo.StartDamage();
    }

    public void AddComboDamage(int damage)
    {
        playerCombo.AddDamage(damage);
    }

    public void TargetActor(CombatActor actor)
    {
        targetReticle.Track(actor);
    }

    public void HideTarget()
    {
        targetReticle.Hide();
    }

    public void ShowAbilityList(PlayerActor player)
    {
        abilityList.PopulateList(player.GetAllUnlockedTechs());
        abilityList.gameObject.SetActive(true);
    }

    public void ShowItemList(CombatMoveBase[] itemList, int[] itemQuantities)
    {
        abilityList.PopulateList(itemList, itemQuantities);
        abilityList.gameObject.SetActive(true);
    }

    public void HideAbilityList()
    {
        abilityList.gameObject.SetActive(false);
    }
}
