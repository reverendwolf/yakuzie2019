﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VSMPlayerControls : MonoBehaviour
{
    public enum ControlsSet { Combat, Melee, List, Emtpy, Walking}

    [SerializeField]
    VSMButtonControl triangle;
    [SerializeField]
    VSMButtonControl square;
    [SerializeField]
    VSMButtonControl cross;
    [SerializeField]
    VSMButtonControl circle;
    [SerializeField]
    VSMButtonControl l1;
    [SerializeField]
    VSMButtonControl r1;

    public void SetContols(ControlsSet set)
    {
        switch(set)
        {
            case ControlsSet.Combat:
                triangle.ButtonShow("Abilities");
                square.ButtonShow("Melee");
                cross.ButtonShow("", false);
                circle.ButtonShow("Items");
                l1.ButtonShow("", false);
                r1.ButtonShow("", false);
                break;
            case ControlsSet.Melee:
                triangle.ButtonShow("Alternate");
                square.ButtonShow("Primary");
                cross.ButtonShow("Special");
                circle.ButtonShow("Finish");
                l1.ButtonShow("", false);
                r1.ButtonShow("", false);
                break;
            case ControlsSet.List:
                triangle.ButtonShow("", false);
                square.ButtonShow("", false);
                cross.ButtonShow("Select");
                circle.ButtonShow("Cancel");
                l1.ButtonShow("", false);
                r1.ButtonShow("", false);
                break;
            case ControlsSet.Emtpy:
                triangle.ButtonShow("", false);
                square.ButtonShow("", false);
                cross.ButtonShow("", false);
                circle.ButtonShow("", false);
                l1.ButtonShow("", false);
                r1.ButtonShow("", false);
                break;
            case ControlsSet.Walking:
                triangle.ButtonShow("", false);
                square.ButtonShow("", false);
                cross.ButtonShow("", false);
                circle.ButtonShow("", false);
                l1.ButtonShow("Sprint", true);
                r1.ButtonShow("", false);
                break;
        }
    }

    public void SetWeaponControls(MoveWeapon weapon)
    {
        triangle.ButtonShow(weapon.SecondaryName);
        square.ButtonShow(weapon.PrimaryName);
        cross.ButtonShow("Special");
        circle.ButtonShow("Cancel");
        l1.ButtonShow("", false);
        r1.ButtonShow("", false);
    }

    public void SetInteractionLabel(string set)
    {
        square.ButtonShow(set);
    }

    public void HideInteractionLabel()
    {
        square.ButtonShow("", false);
    }
}
