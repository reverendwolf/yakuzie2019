﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VSMButtonControl : MonoBehaviour
{
    [SerializeField]
    Text buttonText;
    [SerializeField]
    Image buttonImage;

    Color btc;
    Color bic;

    private void OnEnable()
    {
        btc = buttonText.color;
        bic = buttonImage.color;
    }


    public void ButtonShow(string text, bool enable = true)
    {
        buttonText.text = text;
        ButtonEnable(enable);
    }

    void ButtonEnable(bool enable)
    {
        buttonText.color = new Color(btc.r, btc.g, btc.b, enable ? 1.0f : 0.5f);
        buttonImage.color = new Color(bic.r, bic.g, bic.b, enable ? 1.0f : 0.5f);
    }
}
