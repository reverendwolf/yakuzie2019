﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VSMComboModule : MonoBehaviour {

    [SerializeField]
    Text apLabel;
    [SerializeField]
    Text apCounter;

    [SerializeField]
    Text specialName;
    [SerializeField]
    GameObject specialObject;

    [SerializeField]
    Image[] comboObjects;

    [SerializeField]
    Sprite[] meleeImages;
    [SerializeField]
    Color[] meleeColors;

    int ap;
    int attacks;

    private void Start()
    {

    }

    public void StartCombo(int maxAP)
    {
        apLabel.text = "AP";
        attacks = 0;
        ap = maxAP;
        HideCombo();
        HideSpecial();
        UpdateAP();
        gameObject.SetActive(true);
    }

    void UpdateAP()
    {
        apCounter.text = ap.ToString();
    }

    public void AddAttack(int attackValue)
    {
        ap--;
        UpdateAP();

        comboObjects[attacks].color = meleeColors[attackValue - 1];
        comboObjects[attacks].sprite = meleeImages[attackValue - 1];
        comboObjects[attacks].gameObject.SetActive(true);

        attacks++;
    }

    public void HideCombo()
    {
        for (int i = 0; i < comboObjects.Length; i++)
        {
            comboObjects[i].gameObject.SetActive(false);
        }
        gameObject.SetActive(false);
    }

    public void HideSpecial()
    {
        specialName.text = "";
        specialObject.SetActive(false);
    }

    public void ShowSpecial(string name)
    {
        specialName.text = name;
        specialObject.SetActive(true);
    }

    public void StartDamage()
    {
        ap = 0;
        apLabel.text = "Damage:";
        UpdateAP();
    }

    public void AddDamage(int damage)
    {
        ap += damage;
        UpdateAP();
    }
}
