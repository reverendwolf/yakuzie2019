﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VSMTargetReticle : MonoBehaviour
{
    CombatActor trackingActor;
    [SerializeField] Image fillImage;

    private void Start()
    {
        Hide();
    }
    public void Track(CombatActor actor)
    {
        //Debug.Log("Tracking Started");
        trackingActor = actor;
        fillImage.fillAmount = actor.CurHPPercent;
        transform.position = Camera.main.WorldToScreenPoint(trackingActor.HeartPosition);
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
        trackingActor = null;
    }

    private void Update()
    {
        if(trackingActor != null)
        {
            transform.position = Camera.main.WorldToScreenPoint(trackingActor.HeartPosition);
        }
    }


}
