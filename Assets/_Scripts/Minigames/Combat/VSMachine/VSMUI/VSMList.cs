﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VSMList : MonoBehaviour
{
    [SerializeField]
    VSMListItem[] listItems;
    [SerializeField]
    RectTransform listIndicator;
    [SerializeField]
    UnityEngine.UI.Text description;

    List<string> descriptions;

    [SerializeField]
    int selectedItem;

    public int SelectedItem { get { return selectedItem; } }

    //    string[] debugList = new string[] { "Cirrus", "Socrates", "Particle", "Decible", "Hurricane", "Dolphin", "Tulip", "Mason", "Virtue" };
    //string[] debugList = new string[] { "Cirrus", "Socrates", "Particle", "Decible", "Hurricane", "Dolphin", "Tulip"};
    private void Start()
    {
        //selectedItem = 0;
        //UpdateMarker();

        //PopulateList(debugList);
    }

    public bool SelectUp()
    {
        if(selectedItem > 0)
        {
            selectedItem--;
            UpdateMarker();
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool SelectDown()
    {
        if (selectedItem < listItems.Length - 1 && listItems[selectedItem + 1].IsEmtpy == false)
        {
            selectedItem++;
            UpdateMarker();
            return true;
        }
        else
        {
            return false;
        }
    }

    void UpdateMarker()
    {
        listIndicator.position = listItems[selectedItem].transform.position;
        description.text = descriptions[selectedItem];
    }

    public void PopulateList(CombatMoveBase[] combatMoves)
    {
        //Debug.Log("Populating " + combatMoves.Length + " moves");
        if (descriptions == null) descriptions = new List<string>();
        descriptions.Clear();

        for (int i = 0; i < listItems.Length; i++)
        {
            //if (listItems[i] == null) Debug.Log("NO LIST ITEM?");
            if(combatMoves[i] != null)
            {
                listItems[i].Show(combatMoves[i].ProperName);
                descriptions.Add(combatMoves[i].Description);
            }
            else
            {
                listItems[i].ShowNothing();
            }
        }

        selectedItem = 0;
        UpdateMarker();
    }

    public void PopulateList(CombatMoveBase[] itemRegistry, int[] quantities)
    {
        if (descriptions == null) descriptions = new List<string>();
        descriptions.Clear();

        for (int i = 0; i < listItems.Length; i++)
        {
            if (i < itemRegistry.Length)
            {
                listItems[i].Show(itemRegistry[i].ProperName);
                descriptions.Add(itemRegistry[i].Description);
            }
            else
            {
                listItems[i].ShowNothing();
            }
        }

        selectedItem = 0;
        UpdateMarker();
    }

    /*private void Update()
    {
        if(Input.GetKeyDown(KeyCode.UpArrow))
        {
            SelectUp();
        }

        if(Input.GetKeyDown(KeyCode.DownArrow))
        {
            SelectDown();
        }
    }*/

}
