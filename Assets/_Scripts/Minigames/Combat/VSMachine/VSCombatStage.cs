﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VSCombatStage : MonoBehaviour {

    public Transform playerPosition;
    [SerializeField]
    MonsterActor[] enemies;
    [SerializeField]
    GameObject walls;

    public PlayerActor PlayerActor { get { return player; } }
    PlayerActor player;
    public List<MonsterActor> EnemyActors { get { return enemyTeam; } }
    List<MonsterActor> enemyTeam;

    public LayerMask WallsLayer { get { return walls.layer; } }

    private void Start()
    {
        walls.SetActive(false);
        /*if(gameObject.activeInHierarchy)
        {
            gameObject.SetActive(false);
        }*/
    }

    public MonsterActor[] GetEnemies()
    {
        return enemies;
    }

    public Vector3 RandomPoint()
    {
        return Vector3.zero;
    }

    public Vector3 SafePoint()
    {
        //Debug.DrawRay(footPrint.transform.position, (footPrint.transform.position - (PlayerActor.transform.position - footPrint.transform.position).normalized * (footPrint.size.magnitude * 0.45f)).Flatten(), Color.blue);
        return Vector3.zero;
    }

    public void Engage(PlayerActor playerActor, MonsterActor[] enemies)
    {
        gameObject.SetActive(true);
        player = playerActor;
        RegisterEnemies(enemies);
        //SpawnEnemies(enemies);
        walls.SetActive(false);
    }

    public void RaiseWalls()
    {
        walls.SetActive(true);
    }

    public void Disengage()
    {
        //player.transform.position = playerOriginalPosition;
        //DespawnEnemies();
        walls.SetActive(false);
        //gameObject.SetActive(false);
    }

    public void SetMultiCamera()
    {

    }

    public void ReleaseMultiCamera()
    {

    }

    void RegisterEnemies(MonsterActor[] enemies)
    {
        if (enemyTeam == null) enemyTeam = new List<MonsterActor>();
        enemyTeam.Clear();
        enemyTeam.AddRange(enemies);
    }

    void DespawnEnemies()
    {
        for (int i = 0; i < enemyTeam.Count; i++)
        {
            Destroy(enemyTeam[i].gameObject);
        }
    }
}
