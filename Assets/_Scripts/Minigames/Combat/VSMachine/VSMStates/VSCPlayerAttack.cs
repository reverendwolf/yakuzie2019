﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VSCPlayerAttack : VSCTargetBase
{
    public static List<int> ComboTable = new List<int>()
    {
        1,
        11,
        2,
        111,
        21,
        12,
        3,
        1111,
        211,
        121,
        112,
        22,
        13
    };

    [SerializeField]
    int pointPool;
    [SerializeField]
    int combo; //note: this can't go over 9 digits long or it breaks terribly (we only really need 7)
    int comboDepth;
    [SerializeField]
    List<VSCombatMachine.ActionItem> comboItems;

    float meleeRange;

    public override void AddListeners()
    {
        base.AddListeners();
        InputMaster.OnActionPressed += ButtonInput;
    }

    public override void RemoveListeners()
    {
        base.RemoveListeners();
        InputMaster.OnActionPressed -= ButtonInput;
    }

    public override void Enter()
    {
        base.Enter();
        //Debug.Log("PlayerMelee");
        //VSCM.VSCombatUI.SetControls(VSMPlayerControls.ControlsSet.Melee);
        VSCM.VSCombatUI.SetControls(VSCM.Stage.PlayerActor.Weapon);
        pointPool = VSCM.PlayerActor.MaxAP;
        VSCM.Stage.PlayerActor.Idle();
        comboDepth = 0;
        combo = 0;
        meleeRange = VSCM.PlayerActor.Weapon.Targetting.Range;
        comboItems = new List<VSCombatMachine.ActionItem>();
        VSCM.SetTGT(VSCM.Stage.PlayerActor.transform.position, meleeRange);

        GatherEnemyTargetsInRange(meleeRange);
        if (CurrentTarget != null)
        {
            VSCM.VSCombatUI.ShowCombo();
            VSCM.VSCombatUI.TargetActor(CurrentTarget);
        }
    }

    public override void Exit()
    {
        base.Exit();
        VSCM.HideTGT();
        VSCM.VSCombatUI.HideTarget();
    }


    void ButtonInput(InputMaster.ButtonInput button, InputMaster.ButtonModifier modifier)
    {
        if(button == InputMaster.ButtonInput.Circle)
        {
            //quit, go back to BattleRun
            //or if we have already used some attacks,
            //go to RunQueue
            if(combo != 0)
            {
                //Debug.Log("Run Early Combo");
                SendCombo();
            }
            else
            {
                //Debug.Log("Cancel Combo");
                VSCM.VSCombatUI.HideCombo();
                VSCM.ChangeState<VSCBattleRun>();
            }
            
        }

        if(button == InputMaster.ButtonInput.Square)
        {
            //light
            AttackInput(1);
        }

        if(button == InputMaster.ButtonInput.Triangle)
        {
            //heavy
            AttackInput(2);
        }

        if(button == InputMaster.ButtonInput.Cross)
        {
            //special
            AttackInput(3);
        }
    }

    void AttackInput(int score)
    {
        if (CurrentTarget == null)
            return;

        if(pointPool <= 0)
        {
            Debug.Log("NO AP");
            return;
        }
        else
        {
            switch(score)
            {
                case 1:
                    comboItems.Add(new VSCombatMachine.ActionItem(VSCombatMachine.ActionItem.ActionType.Melee, 0, (comboDepth % 3) + 1));
                    break;
                case 2:
                    comboItems.Add(new VSCombatMachine.ActionItem(VSCombatMachine.ActionItem.ActionType.Melee, 1, (comboDepth % 3) + 4));
                    break;
                case 3:
                    
                    //Debug.Log("DB Check:" + (comboTable.Contains(combo) ? " YES: " + comboTable.IndexOf(combo) : " NO."));
                    if (ComboTable.Contains(combo) && HasDeathblow(combo))
                    {
                        comboItems.Add(new VSCombatMachine.ActionItem(VSCombatMachine.ActionItem.ActionType.Deathblow, ComboTable.IndexOf(combo), 1));
                        
                    }
                    else
                    {
                        comboItems.Add(new VSCombatMachine.ActionItem(VSCombatMachine.ActionItem.ActionType.Melee, 2, 7));
                    }
                    VSCM.VSCombatUI.AddComboAttack(score);
                    SendCombo();
                    return;
            }
        }

        VSCM.VSCombatUI.AddComboAttack(score);

        

        pointPool--;
        int comboAdd = (int)Mathf.Pow(10, comboDepth) * score;
        combo += comboAdd;
        comboDepth++;

        if (pointPool >= 3 && ComboTable.Contains(combo) && HasDeathblow(combo))
        {
            VSCM.VSCombatUI.ShowSpecial(GetDeathblow(combo).ProperName);
        }
        else
        {
            VSCM.VSCombatUI.HideSpecial();
        }

        if (pointPool == 0)
        {
            SendCombo();
        }
    }

    void SendCombo()
    {
        VSCM.Stage.PlayerActor.BattleData.AP = pointPool;
        VSCM.ChangeState<VSCPerformAttack>();
        GetComponent<VSCPerformAttack>().PerformMeleeCombo(VSCM.PlayerActor, comboItems, CurrentTarget);
    }

    bool HasDeathblow(int slot)
    {
        return VSCM.Stage.PlayerActor.HasDeathblowInSlot((MonsterDeathblowUpgrade.DeathblowList)ComboTable.IndexOf(slot));
    }

    CombatMoveBase GetDeathblow(int slot)
    {
        return VSCM.Stage.PlayerActor.GetDeathblowInSlot((MonsterDeathblowUpgrade.DeathblowList)ComboTable.IndexOf(slot));
    }
}
