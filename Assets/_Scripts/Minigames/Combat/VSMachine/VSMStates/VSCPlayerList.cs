﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VSCPlayerList : VSCTargetBase
{
    public enum ListMode
    {
        Ability, Item
    }

    ListMode listMode;
    [SerializeField]
    CombatMoveBase selectedAbility;
    [SerializeField]
    List<CombatActor> actorsHit;
    public override void Enter()
    {
        base.Enter();
        //GatherAllEnemyTargets();
        VSCM.VSCombatUI.SetControls(VSMPlayerControls.ControlsSet.List);

        //VSCM.Stage.SetCloseCamera();

        
    }

    public override void Exit()
    {
        base.Exit();
        selectedAbility = null;
        VSCM.VSCombatUI.HideTarget();
        HideAllLists();
        VSCM.HideTGT();
    }

    void GatherListItemTargets()
    {/*
        VSCM.HideTGT();
        VSCM.VSCombatUI.HideTarget();
        if (listMode == ListMode.Ability)
            selectedAbility = VSCM.PlayerActor.CharacterData.Template.TechList[VSCM.VSCombatUI.AbilityList.SelectedItem].Move;
        else if (listMode == ListMode.Item)
            selectedAbility = GameMasterMachine.I.Settings.ItemDatabase[VSCM.VSCombatUI.AbilityList.SelectedItem];

        if (selectedAbility.Targetting.TargetType == CombatMoveBase.TargetSettings.Target.Foe)
        {
            if (selectedAbility.Targetting.TargetShape == CombatMoveBase.TargetSettings.Shape.Melee)
            {
                VSCM.SetTGT(VSCM.Stage.PlayerActor.transform.position, selectedAbility.Targetting.Range);
                GatherEnemyTargetsInRange(selectedAbility.Targetting.Range);
            }
            else
            {
                GatherAllEnemyTargets();
            }
        }
        else if (selectedAbility.Targetting.TargetType == CombatMoveBase.TargetSettings.Target.Self)
        {
            GatherFriendlyTargets();
        }

        if (selectedTarget >= 0)
        {
            VSCM.VSCombatUI.TargetActor(CurrentTarget);
        }

        GetActorsHitByAbility();*/
    }

    void GetActorsHitByItem()
    {
        if (actorsHit == null) actorsHit = new List<CombatActor>();
        actorsHit.Clear();
        GatherFriendlyTargets();
        actorsHit.AddRange(actorTargets);
    }

    void GetActorsHitByAbility()
    {
        if (actorsHit == null) actorsHit = new List<CombatActor>();
        actorsHit.Clear();

        switch(selectedAbility.Targetting.TargetShape)
        {
            case CombatMoveBase.TargetSettings.Shape.Ray:
                Vector3 dir = actorTargets[selectedTarget].transform.position - VSCM.PlayerActor.transform.position;
                RaycastHit[] hits = Physics.SphereCastAll(VSCM.PlayerActor.transform.position, selectedAbility.Targetting.Range, dir, dir.magnitude, VSCM.ActorMask);
                for (int i = 0; i < hits.Length; i++)
                {
                    //Debug.Log("Found: " + hits[i].collider.gameObject.name);
                    if (actorTargets.Contains(hits[i].collider.GetComponent<CombatActor>()))
                        actorsHit.Add(hits[i].collider.GetComponent<CombatActor>());
                }
                break;
            case CombatMoveBase.TargetSettings.Shape.Sphere:
                Collider[] colliders = Physics.OverlapSphere(actorTargets[selectedTarget].transform.position, selectedAbility.Targetting.Range, VSCM.ActorMask);
                for (int i = 0; i < colliders.Length; i++)
                {
                    if(actorTargets.Contains(colliders[i].GetComponent<CombatActor>()))
                    {
                        actorsHit.Add(colliders[i].GetComponent<CombatActor>());
                    }
                }
                break;
            case CombatMoveBase.TargetSettings.Shape.Single:
            case CombatMoveBase.TargetSettings.Shape.Melee:
                if(actorTargets.Count > 0 && selectedTarget >= 0)
                    actorsHit.Add(actorTargets[selectedTarget]);
                break;
        }
    }

    private void OnDrawGizmos()
    {
        if(selectedAbility != null)
        {
            switch(selectedAbility.Targetting.TargetShape)
            {
                case CombatMoveBase.TargetSettings.Shape.Melee:
                    Gizmos.color = Color.black;
                    Gizmos.DrawWireSphere(VSCM.PlayerActor.transform.position, selectedAbility.Targetting.Range);
                    break;
                case CombatMoveBase.TargetSettings.Shape.Ray:
                    Gizmos.color = Color.red;
                    Vector3 start = VSCM.PlayerActor.transform.position + Vector3.up;
                    Vector3 end = actorTargets[selectedTarget].transform.position + Vector3.up;
                    Vector3 dir = (end - start);
                    Gizmos.DrawLine(start, end);
                    //Gizmos.DrawWireSphere(start, selectedAbility.Targetting.Range);
                    Gizmos.DrawWireSphere(end, selectedAbility.Targetting.Range);
                    for (int i = 0; i < 10; i++)
                    {
                        Gizmos.DrawWireSphere(start + ((dir / 10f) * i), selectedAbility.Targetting.Range);
                    }
                    break;
                case CombatMoveBase.TargetSettings.Shape.Single:
                    Gizmos.color = Color.yellow;
                    Gizmos.DrawWireCube(actorTargets[selectedTarget].transform.position + Vector3.up, Vector3.one * 0.5f);
                    break;
                case CombatMoveBase.TargetSettings.Shape.Sphere:
                    Gizmos.color = Color.green;
                    Gizmos.DrawWireSphere(actorTargets[selectedTarget].transform.position, selectedAbility.Targetting.Range);
                    break;
            }

            if(actorsHit != null)
            {
                for (int i = 0; i < actorsHit.Count; i++)
                {
                    Gizmos.color = Color.black;
                    Gizmos.DrawWireCube(actorsHit[i].transform.position + Vector3.up, Vector3.one * 0.35f);
                    Gizmos.DrawWireCube(actorsHit[i].transform.position + Vector3.up, Vector3.one * 0.30f);
                    Gizmos.DrawWireCube(actorsHit[i].transform.position + Vector3.up, Vector3.one * 0.25f);
                }
            }
        }
    }

    public override void AddListeners()
    {
        base.AddListeners();
        InputMaster.OnActionPressed += ButtonInput;
    }

    public override void RemoveListeners()
    {
        base.RemoveListeners();
        InputMaster.OnActionPressed -= ButtonInput;
    }

    CombatActor[] GetTargets(CombatMoveBase move)
    {
        //eventually this will need to take into consideration the currently selected ability/item
        //and it's targetting methods. for now everything is just treated as single target
        //selectedAbility = move;
        List<CombatActor> _return = new List<CombatActor>();
        //GetActorsHitByAbility(selectedAbility);
        for (int i = 0; i < actorsHit.Count; i++)
        {
            _return.Add(actorsHit[i]);
        }
        //_return.Add(VSCM.FindCardWithActor(actorTargets[selectedTarget]));

        return _return.ToArray();
    }

    void ButtonInput(InputMaster.ButtonInput button, InputMaster.ButtonModifier modifier)
    {
        if(button == InputMaster.ButtonInput.Cross)
        {
            if(listMode == ListMode.Ability)
            {
                if (VSCM.PlayerActor.CurSPBars >= selectedAbility.GetCost() && selectedTarget >= 0)
                {
                    VSCM.ChangeState<VSCPerformAttack>();
                    VSCM.GetComponent<VSCPerformAttack>().PerformAbility(VSCM.PlayerActor, 
                        new VSCombatMachine.ActionItem(VSCombatMachine.ActionItem.ActionType.Tech,  VSCM.VSCombatUI.AbilityList.SelectedItem, 1), 
                        GetTargets(selectedAbility));
                }
                else
                {
                    if (VSCM.PlayerActor.CurSPBars < selectedAbility.GetCost())
                        Debug.Log("Cannot Afford " + selectedAbility.ProperName + ". Cost: " + selectedAbility.Settings.Cost.ToString());
                    else if (selectedTarget < 0)
                        Debug.Log("No valid targets available");
                }
                
            }
            else
            {
                if(VSCM.GameData.ConsumableBag[VSCM.VSCombatUI.AbilityList.SelectedItem] > 0 && selectedTarget >= 0)
                {
                    VSCM.ChangeState<VSCPerformAttack>();
                    //VSCM.GetComponent<VSCPerformAttack>().PerformItem(VSCM.PlayerCard, selectedItem);
                    VSCM.GetComponent<VSCPerformAttack>().PerformAbility(VSCM.PlayerActor,
                        new VSCombatMachine.ActionItem(VSCombatMachine.ActionItem.ActionType.Item, VSCM.VSCombatUI.AbilityList.SelectedItem, 1),
                        GetTargets(selectedAbility));
                }
                else
                {
                    if (selectedTarget < 0)
                        Debug.Log("No valid targets available");
                    else
                        Debug.Log("Item resource depleted!");
                }
            }
        }

        if (button == InputMaster.ButtonInput.Circle)
        {
            VSCM.ChangeState<VSCBattleRun>();
            //VSCM.Stage.ReleaseCloseCamera();
        }

        if( button == InputMaster.ButtonInput.Up)
        {
            VSCM.VSCombatUI.AbilityList.SelectUp();
            GatherListItemTargets();
        }

        if(button == InputMaster.ButtonInput.Left || button == InputMaster.ButtonInput.Right)
        {
            GetActorsHitByAbility();
        }

        if(button == InputMaster.ButtonInput.Down)
        {
            VSCM.VSCombatUI.AbilityList.SelectDown();
            GatherListItemTargets();
        }
    }

    public void ShowAbilityList()
    {
        listMode = ListMode.Ability;
        VSCM.VSCombatUI.ShowAbilityList(VSCM.PlayerActor);
        GatherListItemTargets();
        if (selectedTarget >= 0)
        {
            VSCM.VSCombatUI.TargetActor(CurrentTarget);
        }
    }

    public void ShowItemList()
    {
        listMode = ListMode.Item;
        VSCM.VSCombatUI.ShowItemList(GameMasterMachine.I.Settings.ItemDatabase, VSCM.GameData.ConsumableBag);
        GatherListItemTargets();
        if (selectedTarget >= 0)
        {
            VSCM.VSCombatUI.TargetActor(CurrentTarget);
        }
    }

    void HideAllLists()
    {
        VSCM.VSCombatUI.HideAbilityList();
        
    }
}
