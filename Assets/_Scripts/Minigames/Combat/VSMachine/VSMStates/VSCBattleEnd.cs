﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VSCBattleEnd : VSCBase
{

    public override void Enter()
    {
        base.Enter();
        Debug.Log("VS-BattleEnd");
        StartCoroutine(Victory());
        GM.ShowControls(false);
    }

    IEnumerator Victory()
    {
        yield return null;
        VSCM.Stage.PlayerActor.DoGesture(CombatActor.Gesture.CombatVictory);
        yield return new WaitForSeconds(5.75f);
        VSCM.Stage.PlayerActor.ReleaseGesture();
        VSCM.Stage.Disengage();
        VSCM.EndCombat(0);
        
    }

    IEnumerator Defeat()
    {
        yield return null;
        yield return StartCoroutine(GameMasterMachine.I.UI.ColorFade(Color.black, 5f));
        VSCM.Stage.Disengage();
        VSCM.EndCombat(1);
    }
}
