﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VSCBase : MBBaseState
{
    VSCombatMachine vscm;
    protected VSCombatMachine VSCM { get { return vscm; } }
    protected GameMasterMachine GM { get { return GameMasterMachine.I; } }

    public override void Enter()
    {
        base.Enter();
        vscm = GetComponent<VSCombatMachine>();
    }

}
