﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VSCBattleStart : VSCBase
{

    public override void Enter()
    {
        base.Enter();
        Debug.Log("VSC-Start");
        //StartCoroutine(StartBattle());
    }

    public void StartBattle(int battleCode, MonsterActor[] enemies, string combatName)
    {
        StartCoroutine(SetupBattle(battleCode, enemies, combatName));
    }

    IEnumerator SetupBattle(int battleCode, MonsterActor[] enemies, string combatName)
    {
        yield return null;
        Debug.Log("Begin: " + combatName);
        //VSCM.VSCombatUI.ShowTitleCard(combatName);
        VSCM.Stage.Engage(GameMasterMachine.I.PlayerActor, enemies);
        VSCM.Stage.PlayerActor.WalkToPosition(VSCM.Stage.playerPosition.position);
        yield return null;
        while(VSCM.Stage.PlayerActor.IsWalking)
        {
           // Debug.Log("IsWalking");
            yield return null;
        }
        /*
        Vector3 direction = VSCM.Stage.playerPosition.position - VSCM.Stage.PlayerActor.transform.position;
        float dSquare = Vector3.SqrMagnitude(direction);
        while (dSquare > 0.05f)
        {
            float lastDistance = dSquare;
            VSCM.Stage.PlayerActor.WalkDirection(new Vector2(direction.x, direction.z).normalized, 1f);
            dSquare = Vector3.SqrMagnitude(VSCM.Stage.playerPosition.position - VSCM.Stage.PlayerActor.transform.position);
            if (dSquare > lastDistance) dSquare = 0f;
            yield return new WaitForEndOfFrame();
        }
        VSCM.Stage.PlayerActor.WalkDirection(Vector2.zero, 0);
        */

        VSCM.InitializePlayer(GameMasterMachine.I.PlayerActor);
        VSCM.InitializeEnemies(enemies);
        

        VSCM.Stage.RaiseWalls();
        while(VSCM.VSCombatUI.ShowingTitle)
        {
            yield return null;
        }
        yield return new WaitForSecondsRealtime(1f);
        VSCM.ChangeState<VSCBattleRun>();
    }
}
