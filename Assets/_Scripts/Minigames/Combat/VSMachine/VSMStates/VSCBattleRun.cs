﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VSCBattleRun : VSCBase
{
    bool running;

    bool Ready { get { return wait <= 0f; } }

    float waitTime = 0.15f;
    float wait;

    public override void AddListeners()
    {
        base.AddListeners();
        InputMaster.OnActionPressed += ButtonInput;
        InputMaster.LeftStickRelativeDirection += StickInput;
    }

    public override void RemoveListeners()
    {
        base.RemoveListeners();
        InputMaster.OnActionPressed -= ButtonInput;
        InputMaster.LeftStickRelativeDirection -= StickInput;
    }

    public override void Enter()
    {
        base.Enter();
        //Debug.Log("VSC-Run");
        wait = waitTime;
        VSCM.ResumeCombatAnimations();
        VSCM.VSCombatUI.SetControls(VSMPlayerControls.ControlsSet.Combat);
        //Debug.Log("BattleStatus: " + VSCM.BattleStatus());
        if (VSCM.BattleStatus() == 0) StartRunning();
        else Invoke("EndBattle", waitTime);
    }

    public override void Exit()
    {
        base.Exit();
        VSCM.Stage.PlayerActor.Idle();
        if (running) running = false;
    }

    void StartRunning()
    {
        running = true;
    }

    void Update()
    {
        if (wait > 0f) wait -= Time.deltaTime;
        if(running)
        {
            ChargeBattleCT();
            TickEnemyAI();
        }
    }

    void ChargeBattleCT()
    {
        float delta = Time.deltaTime;
        VSCM.PlayerActor.BattleData.ChargeCT(delta);
        for (int i = 0; i < VSCM.CombatAI.Count; i++)
        {
            MonsterBattleData bd = VSCM.CombatAI[i].Actor.BattleData;
            if (!bd.CTFull)
            {
                bd.ChargeCT(delta * 0.2f);

                if(bd.CTFull) VSCM.CombatAI[i].Actor.Emote.PopEmote(ActorEmote.Emote.DoubleExclamation);
            }
        }
    }

    void TickEnemyAI()
    {
        for (int i = 0; i < VSCM.CombatAI.Count; i++)
        {
            VSCombatAI ai = VSCM.CombatAI[i];
            CombatActor actor = ai.Actor;
            if (actor.BattleData.BattleStance != MonsterBattleData.Stance.KO)
            {
                ai.Tick();
                /*
                if(ai.AttackReady == true && Ready)
                {
                    if(ai.SelectedType == VSCombatAI.MoveTypeSelected.Combo)
                    {
                        AIAttack(actor, VSCM.PlayerActor, actor.CharacterData.Weapon);
                    }
                    else
                    {

                    }
                    ai.ForceThink();
                    //Debug.Log("AI ATTACK TEST");
                    
                }
                */
            }
        }
    }

    void EndBattle()
    {
        VSCM.ChangeState<VSCBattleEnd>();
    }

    void ButtonInput(InputMaster.ButtonInput button, InputMaster.ButtonModifier modifier)
    {
        if (VSCM.PlayerActor.BattleData.CTFull && Ready)
        {
            running = false;
            if (button == InputMaster.ButtonInput.Square)
            {
                VSCM.ChangeState<VSCPlayerAttack>();
            }

            if(button == InputMaster.ButtonInput.Triangle)
            {
                VSCM.ChangeState<VSCPlayerList>();
                VSCM.GetComponent<VSCPlayerList>().ShowAbilityList();
            }

            if(button == InputMaster.ButtonInput.Circle)
            {
                VSCM.ChangeState<VSCPlayerList>();
                VSCM.GetComponent<VSCPlayerList>().ShowItemList();
            }
        }
        
    }

    void StickInput(Vector2 direction)
    {
        VSCM.Stage.PlayerActor.CombatWalkDirection(direction, direction.magnitude);
    }

    public void AIAttack(CombatActor attacker, CombatActor defender, CombatMoveBase move)
    {
        if (VSCM.CurrentState == this && running)
        {
            running = false;
            VSCM.ChangeState<VSCPerformAttack>();
            VSCM.GetComponent<VSCPerformAttack>().PerformMeleeCombo(attacker, new List<VSCombatMachine.ActionItem>() { new VSCombatMachine.ActionItem(VSCombatMachine.ActionItem.ActionType.Melee, 1, Random.Range(1,10)) }, defender);
        }
    }
}
