﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class VSCTargetBase : VSCBase
{
    const int PIXELBUFFER = 0;

    bool stickInputLock;

    [SerializeField]
    protected int selectedTarget;

    [SerializeField]
    protected List<CombatActor> actorTargets;
    
    [SerializeField]
    List<int> targetsByX;
    [SerializeField]
    int xIndex;

    protected CombatActor CurrentTarget { get { if(selectedTarget < 0) return null; else return actorTargets[selectedTarget]; } }

    public override void AddListeners()
    {
        base.AddListeners();
        InputMaster.OnActionPressed += ButtonInput;
        InputMaster.LeftStickDirection += StickInput;
    }

    public override void RemoveListeners()
    {
        base.RemoveListeners();
        InputMaster.OnActionPressed -= ButtonInput;
        InputMaster.LeftStickDirection -= StickInput;
    }

    public override void Enter()
    {
        base.Enter();
        selectedTarget = 0;
        VSCM.PauseCombatAnimations();
        
    }

    void StickInput(Vector2 inputDirection)
    {
        //Debug.Log(inputDirection);
        if (stickInputLock == false && inputDirection.x < -0.35f)
        {
            stickInputLock = true;
            SwitchTargetLeft();
        }

        if (stickInputLock == false && inputDirection.x > 0.35f)
        {
            stickInputLock = true;
            SwitchTargetRight();
        }

        if (Mathf.Abs(inputDirection.x) < 0.1f && stickInputLock == true)
        {
            stickInputLock = false;
        }
    }


    void ButtonInput(InputMaster.ButtonInput button, InputMaster.ButtonModifier modifier)
    {
        if (button == InputMaster.ButtonInput.Left)
        {
            SwitchTargetLeft();
            /*if(selectedTarget >= 0)
            {
                selectedTarget = Mathf.Clamp(selectedTarget - 1, 0, actorTargets.Count);
                VSCM.VSCombatUI.TargetCard(CurrentTarget);
            }*/
        }

        if (button == InputMaster.ButtonInput.Right)
        {
            SwitchTargetRight();
            /*if (selectedTarget < actorTargets.Count - 1)
            {
                selectedTarget = Mathf.Clamp(selectedTarget + 1, 0, actorTargets.Count);
                VSCM.VSCombatUI.TargetCard(CurrentTarget);
            }*/
        }
    }

    protected void GatherEnemyTargetsInRange(float targetRange) //will need to be abstracted?
    {
        //Debug.Log("Gather Targets in Range: " + targetRange);
        InitializeGather();

        Collider[] potentialTargets = Physics.OverlapSphere(VSCM.Stage.PlayerActor.transform.position, targetRange, VSCM.ActorMask);

        if (potentialTargets.Length > 1)
        {
            Debug.Log((potentialTargets.Length - 1) + " Actors Found");
            for (int i = 0; i < potentialTargets.Length; i++)
            {
                if (potentialTargets[i].tag == "Enemy") actorTargets.Add(potentialTargets[i].GetComponent<CombatActor>());
            }

            if (actorTargets.Count < 1)
                selectedTarget = -1;
            /*for (int i = 0; i < actorTargets.Count; i++)
            {
                Debug.Log("Actor Found:" + actorTargets[i].name);
            }*/
        }
        else
        {
            selectedTarget = -1;
            Debug.Log("No Targets Found");
        }

        if (selectedTarget >= 0)
        {
            VSCM.VSCombatUI.TargetActor(CurrentTarget);
            VSCM.TGT.SetActive(true);
        }
            
    }

    protected void GatherAllEnemyTargets(bool requireLiving = true) //will need to be abstracted?
    {
        //Debug.Log("Gather All Enemy Targets");
        InitializeGather();

        for (int i = 0; i < VSCM.CombatAI.Count; i++)
        {
            if((requireLiving && VSCM.CombatAI[i].Actor.CurHP > 0) || requireLiving == false)
            {
                actorTargets.Add(VSCM.CombatAI[i].Actor);
            }
        }

        if (actorTargets.Count < 1)
        {
            selectedTarget = -1;
        }

        if(CurrentTarget != null) VSCM.VSCombatUI.TargetActor(CurrentTarget);
    }

    protected void GatherFriendlyTargets()
    {
        //Debug.Log("Gather Friendly Targets");
        InitializeGather();

        actorTargets.Add(VSCM.PlayerActor);

        if(actorTargets.Count < 1)
        {
            selectedTarget = -1;
        }

        if (CurrentTarget != null) VSCM.VSCombatUI.TargetActor(CurrentTarget);
    }

    void InitializeGather()
    {
        selectedTarget = 0;

        
        if (actorTargets == null) actorTargets = new List<CombatActor>();
        actorTargets.Clear();
    }



    void OrderTargetsByX()
    {
        //put targets in order of their X position on screen
        //so we can navigate with L/R input
        if (targetsByX == null) targetsByX = new List<int>();
        targetsByX.Clear();

        List<int> allTargets = new List<int>();
        for (int i = 0; i < actorTargets.Count; i++)
        {
            allTargets.Add(i);
        }

        while (allTargets.Count > 0)
        {
            float minX = float.MaxValue;
            int curIndex = -1;

            for (int i = 0; i < allTargets.Count; i++)
            {
                Vector3 screenPosition = Camera.main.WorldToScreenPoint(actorTargets[allTargets[i]].transform.position);
                if (screenPosition.x < minX)
                {
                    minX = screenPosition.x;
                    curIndex = i;
                }
            }
            targetsByX.Add(allTargets[curIndex]);
            allTargets.RemoveAt(curIndex);
        }
    }

    bool IsTargetOnScreen(CombatActor actor)
    {
        Vector3 ap = Camera.main.WorldToScreenPoint(actor.transform.position);
        return ((ap.x > (0 + PIXELBUFFER) && ap.x < (Screen.width - PIXELBUFFER)) &&
                (ap.y > (0 + PIXELBUFFER) && ap.y < (Screen.height - PIXELBUFFER)));
    }

    protected void SwitchTargetLeft()
    {
        if (selectedTarget < 0)
            return;

        OrderTargetsByX();
        xIndex = targetsByX.IndexOf(selectedTarget);
        if (xIndex > 0 && IsTargetOnScreen(actorTargets[targetsByX[xIndex - 1]]))
        {
            selectedTarget = targetsByX[xIndex - 1];
            VSCM.VSCombatUI.TargetActor(CurrentTarget);
        }
    }

    protected void SwitchTargetRight()
    {
        if (selectedTarget < 0)
            return;

        OrderTargetsByX();
        xIndex = targetsByX.IndexOf(selectedTarget);
        if (xIndex < targetsByX.Count - 1 && IsTargetOnScreen(actorTargets[targetsByX[xIndex + 1]]))
        {
            selectedTarget = targetsByX[xIndex + 1];
            VSCM.VSCombatUI.TargetActor(CurrentTarget);
        }
    }
}
