﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VSCPerformAttack : VSCBase
{
    bool criticalAttack;

    List<CombatMoveBase.CombatMessage> combatMessages;

    public override void Enter()
    {
        base.Enter();
        if (combatMessages == null) combatMessages = new List<CombatMoveBase.CombatMessage>();
        combatMessages.Clear();
    }

    public override void Exit()
    {
        base.Exit();
        VSCM.VSCombatUI.HideCombo();
    }

    public void PerformMeleeCombo(CombatActor attacker, List<VSCombatMachine.ActionItem> comboItems, CombatActor target)
    {
        //Debug.Log("Player Combo");
        StartCoroutine(RunComboQueue(attacker, comboItems, target));
    }

    IEnumerator RunComboQueue(CombatActor attacker, List<VSCombatMachine.ActionItem> comboItems, CombatActor target)
    {
        Begin(attacker);
        target.PauseAnimation(false);
        MoveWeapon weapon = attacker.Weapon;
        yield return null;
        //Debug.Log(attacker.cardName + " vs " + target.cardName);
        attacker.TurnToFace(target.transform.position);
        target.Idle();
        for (int i = 0; i < comboItems.Count; i++)
        {
            //Debug.Log("Combo " + i + ": " + comboItems[i].actionType.ToString() + " (" + comboItems[i].actionSelection + ")");
            //determine the effect of the attack
            if (comboItems[i].actionType == VSCombatMachine.ActionItem.ActionType.Melee)
            {
                combatMessages.Add(comboItems[i].actionSelection == 3 ? weapon.HeavyAttack(attacker, target) : weapon.NormalAttack(attacker, target));
                //Debug.Log("Added Message: " + combatMessages[combatMessages.Count - 1].ToString());
                //Debug.Log("Total Messages: " + combatMessages.Count);
            }
            else if (comboItems[i].actionType == VSCombatMachine.ActionItem.ActionType.Deathblow)
            {
                //this will only happen with the player. monsters don't get deathblows
                CombatMoveBase deathblow = ((PlayerActor)attacker).GetDeathblowInSlot((MonsterDeathblowUpgrade.DeathblowList)comboItems[i].actionSelection);
                VSCM.VSCombatUI.ShowMoveCard(deathblow.ProperName);
                combatMessages.Add(deathblow.Execute(attacker, target));
            }
            attacker.AnimationMessenger.OnHit += AttackHit;
            attacker.AnimationMessenger.AttackFinished += AttackFinished;
            //Debug.Log("DoCombat Start " + Time.time);
            yield return attacker.DoCombatAction(comboItems[i]);
            //Debug.Log("DoCombat End " + Time.time);
            attacker.BattleUpdate();
            //yield return new WaitForSecondsRealtime(1.5f);
            attacker.AnimationMessenger.OnHit -= AttackHit;
            attacker.AnimationMessenger.AttackFinished -= AttackFinished;

            if (target.CurHP <= 0)
            {
                target.Die(true);
                break;
            }
        }

        //yield return DeathCheck(target);
        End(attacker);
        //Debug.Log("Combo Complete");

    }

    /*IEnumerator DeathCheck(CombatActor target)
    {
        if (target.hpCur <= 0)
        {
            target.actor.Die(true);
            yield return new WaitForSeconds(1.25f);
        }
    }*/

    void Begin(CombatActor attacker)
    {
        //if (VSCM == null) Debug.Log("NO VSCM");
        //else Debug.Log("VSCM OK");
        VSCM.PauseCombatAnimations();
        attacker.PauseAnimation(false);
        combatMessages.Clear();
    }

    void End(CombatActor attacker)
    {
        VSCM.ResumeCombatAnimations();
        attacker.BattleData.ClearCT();
        VSCM.ChangeState<VSCBattleRun>();
    }

    void AttackHit(CombatActor.HitReaction reaction)
    {
        //Debug.Log("HIT");
        for (int i = 0; i < combatMessages.Count; i++)
        {
            CombatMoveBase.CombatMessage combatMessage = combatMessages[i];

            if (combatMessage.hit == CombatMoveBase.CombatMessage.Hit.Miss)
                combatMessage.target.HitReact(CombatActor.HitReaction.Dodge);
            else if (combatMessage.hit != CombatMoveBase.CombatMessage.Hit.Heal)
            {
                if (combatMessage.target.CurHP > 0)
                {
                    combatMessage.target.HitReact(reaction);
                    GameMasterMachine.I.HitStop();
                }
                else
                {
                    GameMasterMachine.I.HitSlow();
                    combatMessage.target.Die(true);
                }
                    
            }
        }
        //Debug.Log("Attack Payload:" + ap.power);

        //combatMessages.Clear();
    }

    void AttackFinished()
    {
        //Debug.Log("HITFINISHED");
        for (int i = 0; i < combatMessages.Count; i++)
        {
            CombatMoveBase.CombatMessage combatMessage = combatMessages[i];

            Color c = Color.white;
            string m = combatMessage.number.ToString();
            bool s = true;
            if(combatMessage.hit == CombatMoveBase.CombatMessage.Hit.Critical)
            {
                c = new Color(0.95f, 0.95f, 0.4f);
            }
            else if(combatMessage.hit == CombatMoveBase.CombatMessage.Hit.Heal)
            {
                c = new Color(0.5f, 0.9f, 0.5f);
            }
            else if(combatMessage.hit != CombatMoveBase.CombatMessage.Hit.Normal)
            {
                c = new Color(0.7f, 0.7f, 0.7f);
            }

            if(combatMessage.hit == CombatMoveBase.CombatMessage.Hit.Immune)
            {
                m = "Immune!";
                s = false;
            }
            else if (combatMessage.hit == CombatMoveBase.CombatMessage.Hit.Block)
            {
                m = "Block!";
                s = false;
            }
            else if (combatMessage.hit == CombatMoveBase.CombatMessage.Hit.Dodge)
            {
                m = "Dodge!";
                s = false;
            }

            VSCM.VSCombatUI.PopMessage(combatMessage.target, m, c, s);
        }
        combatMessages.Clear();
    }

    public void PerformAbility(CombatActor attacker, VSCombatMachine.ActionItem moveItem, CombatActor[] targets)
    {
        StartCoroutine(PerformAbilityQueue(attacker, moveItem, targets));
    }

    IEnumerator PerformAbilityQueue(CombatActor attacker, VSCombatMachine.ActionItem moveItem, CombatActor[] targets)
    {
        Debug.Log("Items and Techs Currently Unsupported");
        
        //Debug.Log("Running Ability");
 
        Begin(attacker);
        yield return new WaitForSeconds(1.5f);
        /*
        CombatMoveBase move = moveItem.actionType == VSCombatMachine.ActionItem.ActionType.Tech ? attacker.CharacterData.Template.TechList[moveItem.actionSelection].Move : GameMasterMachine.I.Settings.ItemDatabase[moveItem.actionSelection];

        if (move.Targetting.TargetType == CombatMoveBase.TargetSettings.Target.Self)
        {
            combatMessages.Add(move.Execute(attacker));
        }
        else
        {
            Vector3 avgTargetPosition = Vector3.zero;
            for (int i = 0; i < targets.Length; i++)
            {
                avgTargetPosition += targets[i].transform.position;
                targets[i].PauseAnimation(false);
                targets[i].Idle();
            }
            avgTargetPosition /= targets.Length;

            attacker.TurnToFace(avgTargetPosition);

            combatMessages.AddRange(move.Execute(attacker, targets));
        }

        attacker.AnimationMessenger.OnHit += AttackHit;
        attacker.AnimationMessenger.AttackFinished += AttackFinished;
        VSCM.VSCombatUI.ShowMoveCard(move.ProperName);
        yield return attacker.DoCombatAction(moveItem);
        if (moveItem.actionType == VSCombatMachine.ActionItem.ActionType.Item) AttackFinished();
        if (moveItem.actionType == VSCombatMachine.ActionItem.ActionType.Tech) attacker.CharacterData.StatSheet.PaySPBar(move.GetCost());
        attacker.BattleUpdate();
        attacker.AnimationMessenger.OnHit -= AttackHit;
        attacker.AnimationMessenger.AttackFinished -= AttackFinished;
        */
        End(attacker);
    }

    /*public void PerformItem(CombatCard player, int itemValue)
    {
        StartCoroutine(PerformItemUse(player, itemValue));
    }

    IEnumerator PerformItemUse(CombatCard player, int itemValue)
    {
        Debug.Log("Running Item Use");
        Begin(player);

        VSCombatMachine.ActionItem item = new VSCombatMachine.ActionItem(VSCombatMachine.ActionItem.ActionType.Item, itemValue);
        VSCM.VSCombatUI.ShowMoveCard(GameMasterMachine.I.Settings.ItemRegistry[itemValue].ProperName);
        yield return player.actor.DoAttack(item);

        //VSCM.ItemMaster.UseItem(player, itemValue);
        //VSCM.GameData.ConsumableBag.ConsumeItem(player, itemValue);
        //GameMasterMachine.I.ConsumeItem(player, itemValue);

        player.BattleUpdate();

        End(player);
    }*/
    }
