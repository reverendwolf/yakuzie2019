﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class VSCombatMachine : MBStateMachine
{
    [System.Serializable]
    public struct ActionItem
    {
        public enum ActionType { Melee, Deathblow, Tech, Item}
        public ActionType actionType;
        public int actionSelection;
        public int actionAnimation;
        public bool onSelf;

        public ActionItem(ActionType actionType, int actionSelection, int actionAnimation, bool onSelf = false)
        {
            this.actionType = actionType;
            this.actionSelection = actionSelection;
            this.actionAnimation = actionAnimation;
            this.onSelf = onSelf;
        }
    }

    public static IntegerSignal OnCombatStart;
    public static IntegerSignal OnCombatEnd;

    [SerializeField]
    GameData gameData;
    public GameData GameData { get { return gameData; } }

    //public List<AIController> EnemyAI { get { return enemyAI; } }
    //[SerializeField]
    //List<AIController> enemyAI;
    [SerializeField]
    List<VSCombatAI> combatAI;
    public List<VSCombatAI> CombatAI { get { return combatAI; } }

    public PlayerActor PlayerActor { get { return GameMasterMachine.I.PlayerActor; } }

    public VSCombatStage Stage { get { return combatStage; } }
    VSCombatStage combatStage;

    public GameObject TGT { get { return tgt; } }
    [SerializeField]
    GameObject tgt;

    public VSMUI VSCombatUI { get { return vsCombatUI; } }
    VSMUI vsCombatUI;
    [SerializeField]
    GameObject combatGUI;

    public LayerMask ActorMask { get { return combatActorMask; } }
    [SerializeField]
    LayerMask combatActorMask;

    // Use this for initialization
    void Start()
    {
        ChangeState<VSCBattleIdle>();
    }

    public void StartCombat(int battleCode, MonsterActor[] enemies, string combatName, VSCombatStage stage)
    {
        if (CurrentState is VSCBattleIdle)
        {
            //enemyCache = enemies;
            combatStage = stage;
            vsCombatUI = Instantiate(combatGUI, Vector3.zero, Quaternion.identity, this.transform).GetComponent<VSMUI>();
            GameMasterMachine.I.ShowControls(true);
            ChangeState<VSCBattleStart>();
            //GetComponent<VSCBattleStart>().InitializeBattle(battleCode, enemies, combatName);
            GetComponent<VSCBattleStart>().StartBattle(battleCode, enemies, combatName);
            if (OnCombatStart != null) OnCombatStart(battleCode);
        }
        else
        {
            Debug.LogError("Battle requested while state is not IDLE");
        }

    }

    public void InitializePlayer(CombatActor playerActor)
    {
        //playerActor.BattleData = new MonsterBattleData();
        playerActor.EnterCombat(true);
        vsCombatUI.Initialize(ref playerActor);
        //playerCard.actor.EnterCombat(true);
    }

    public void InitializeEnemies(MonsterActor[] enemyActors)
    {
        //Debug.Log("Initializing " + enemyActors.Length + " enemies.");
        if (this.combatAI == null) this.combatAI = new List<VSCombatAI>();
        this.combatAI.Clear();

        for (int i = 0; i < enemyActors.Length; i++)
        {
            enemyActors[i].Emote.PopEmote(ActorEmote.Emote.Anger);
            enemyActors[i].SetAgentPriority(50);
            enemyActors[i].gameObject.layer = 10;
            enemyActors[i].gameObject.tag = "Enemy";
            enemyActors[i].EnterCombat(true);
            enemyActors[i].BattleData.SetRandomCT();
            this.combatAI.Add(new VSCombatAI(ref enemyActors[i], combatStage, this));
            //this.enemyCards[this.enemyCards.Count - 1].actor.EnterCombat(true);
            //this.enemyAI.Add(enemyActors[i].GetComponent<AIController>());
           // this.enemyAI[this.enemyAI.Count - 1].Initialize(ref newCard, ref combatStage);
        }
    }

    public void ChargeBattleCT()
    {
        
    }

    public void ResetEnemyAI()
    {
        
    }

    public void TickEnemyAI()
    {
        /*for (int i = 0; i < enemyAI.Count; i++)
        {
            if(enemyCards[i].stance == CombatCard.BattleStance.Normal) enemyAI[i].Tick();
        }*/

        for (int i = 0; i < combatAI.Count; i++)
        {
            if (combatAI[i].Actor.BattleData.BattleStance != MonsterBattleData.Stance.KO) combatAI[i].Tick();
        }
    }

    public void PauseCombatAnimations()
    {
        PlayerActor.PauseAnimation(true);

        for (int i = 0; i < combatAI.Count; i++)
        {
            combatAI[i].Actor.PauseAnimation(true);
        }
    }

    public void ResumeCombatAnimations()
    {
        PlayerActor.PauseAnimation(false);

        for (int i = 0; i < combatAI.Count; i++)
        {
            combatAI[i].Actor.PauseAnimation(false);
        }
    }

    public int BattleStatus()
    {
        if(PlayerActor.CurHP > 0)
        {
            for (int i = 0; i < combatAI.Count; i++)
            {
                if (combatAI[i].Actor.CurHP > 0) return 0; //battle continue
            }
            return 1; //battle won
        }
        else
        {
            return 2; //battle lost
        }
    }

    public void EndCombat(int battleCode)
    {
        if (CurrentState is VSCBattleIdle)
            return;

        //Debug.Log("Combat Over:" + battleCode);
        Destroy(vsCombatUI.gameObject);
        PlayerActor.EnterCombat(false);
        for (int i = 0; i < combatAI.Count; i++)
        {
            combatAI[i].Actor.gameObject.layer = 11;
        }
        //(playerCard.character as PlayerData).SetVitals(playerCard);
        if (OnCombatEnd != null) OnCombatEnd(battleCode);
        ChangeState<VSCBattleIdle>();
    }

    /*public CombatCard FindCardWithActor(CombatActor actor)
    {
        if (playerCard.actor == actor)
        {
            //Debug.Log("Found player card");
            return playerCard;
        }
        else
        {
            for (int i = 0; i < enemyCards.Count; i++)
            {
                if (enemyCards[i].actor == actor)
                {
                    //Debug.Log("Found card " + i + " with actor " + actor.name);
                    return enemyCards[i];
                }

            }
        }
        Debug.Log("Found no card");
        return null;
    }*/

    public void SetTGT(Vector3 position, float scale)
    {
        TGT.transform.position = position;
        TGT.transform.localScale = Vector3.one * (scale * 2);
        TGT.SetActive(true);
    }

    public void HideTGT()
    {
        TGT.SetActive(false);
    }
}
