﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Animation))]
public class NumberPopper : MonoBehaviour
{

    bool ready;
    public bool Ready { get { return ready; } }
    [SerializeField] Text text;
    string content;
    //int length;
    float xMove;
    bool shuffle;
    bool move;
    int frameSkip = 0;
    Animation anim;
    Transform tracker;
    Vector3 offsetX;

    private void OnEnable()
    {
        SetReady();
    }

    public void ShowPop(string text, Color color, Transform tracker)
    {
        this.shuffle = false;
        this.text.text = text;
        SetMessage(text, color, tracker);
        Pop();
    }

    public void ScramblePop(string text, Color color, Transform tracker)
    {
        this.shuffle = true;
        SetMessage(text, color, tracker);
        Pop();
    }

    void SetMessage(string text, Color color, Transform tracker)
    {
        this.text.color = color;
        this.content = text;
        this.tracker = tracker;
    }

    void Pop()
    {
        this.ready = false;
        this.anim = GetComponent<Animation>();
        this.xMove = Random.Range(-2f, 2f) * 50f;
        this.offsetX = Vector3.zero;
        this.move = true;
        this.anim.Play();
    }

    public void StopShuffle()
    {
        if(shuffle)
        {
            text.text = content;
            shuffle = false;
            frameSkip = 0;
        }
    }

    public void StopXMove()
    {
        move = false;
    }

    public void SetReady()
    {
        ready = true;
        text.text = "";
    }

    private void Update()
    {
        if (shuffle)
        {
            frameSkip++;
            if (frameSkip % 3 == 0)
            {
                int shuf = Random.Range(10, 99);
                text.text = "" + shuf;
            }
        }

        if (anim != null && anim.isPlaying && move)
        {
            offsetX += new Vector3((xMove * Time.deltaTime), 0, 0);
            transform.position = Camera.main.WorldToScreenPoint(tracker.position) + offsetX;
        }
    }
}
