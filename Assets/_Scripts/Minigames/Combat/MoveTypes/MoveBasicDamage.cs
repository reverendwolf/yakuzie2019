﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CMB-BasicDamage", menuName = "RPG/Combat Moves/Basic Damage")]
public class MoveBasicDamage : CombatMoveBase
{
    [SerializeField]
    CombatMoveBase.StandardDamage damage;

    public override CombatMessage[] Execute(CombatActor attacker, CombatActor[] defenders)
    {
        CombatMessage[] _return = new CombatMessage[defenders.Length];

        for (int i = 0; i < defenders.Length; i++)
        {
            _return[i] = Execute(attacker, defenders[i]);
        }

        return _return;
    }

    public override CombatMessage Execute(CombatActor attacker, CombatActor defender)
    {
        return StandardAttack(attacker, defender, damage);
    }

    public override CombatMessage Execute(CombatActor self)
    {
        CombatMessage _return = new CombatMessage();
        //_return.SetWord("INVALID", Color.red);
        return _return;
    }
}
