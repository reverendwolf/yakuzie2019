﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CMB-BasicHeal", menuName = "RPG/Combat Moves/Basic Heal")]
public class MoveBasicHeal : CombatMoveBase
{
    [SerializeField]
    StandardDamage heal;

    public override CombatMessage Execute(CombatActor attacker, CombatActor defender)
    {
        CombatMessage _return = new CombatMessage();
        _return.hit = CombatMessage.Hit.Heal;
        _return.SetTarget(defender);
        _return.number = heal.RollSuccess(attacker.GetVirtueValue(Settings.MovePower));

        HealCard(defender, _return.number);

        return _return;
    }

    public override CombatMessage[] Execute(CombatActor attacker, CombatActor[] defenders)
    {
        CombatMessage[] _return = new CombatMessage[defenders.Length];

        for (int i = 0; i < _return.Length; i++)
        {
            _return[i] = Execute(attacker, defenders[i]);
        }

        return _return;
    }

    public override CombatMessage Execute(CombatActor self)
    {
        return Execute(self, self);
    }
}
