﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CMB-PercentHeal", menuName = "RPG/Combat Moves/Percent Heal")]
public class MovePercentHeal : CombatMoveBase
{
    [SerializeField]
    [Range(0, 10)]
    int healPercent;

    [SerializeField]
    [Range(0, 10)]
    int specialPercent;

    public override CombatMessage Execute(CombatActor self)
    {
        CombatMessage _return = new CombatMessage();
        int hp = Mathf.CeilToInt(self.MaxHP * (float)(healPercent / 10f));
        int sp = Mathf.CeilToInt(self.MaxSP * (float)(specialPercent / 10f));
        _return.hit = CombatMessage.Hit.Normal;
        if(healPercent > 0)
        {
            _return.number = hp;
        }
        else
        {
            _return.number = sp;
        }
        
        _return.target = self;
        HealCard(self, hp);
        return _return;
    }
}
