﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CMB-Weapon", menuName = "RPG/Combat Moves/Melee Weapon")]
public class MoveWeapon : CombatMoveBase
{
    [SerializeField]
    StandardDamage damage;

    [SerializeField]
    string primaryName;
    [SerializeField]
    string secondaryName;
    public string PrimaryName { get { return primaryName; } }
    public string SecondaryName { get { return secondaryName; } }

    public CombatMessage NormalAttack(CombatActor attacker, CombatActor defender)
    {
        Debug.Log("Normal!");
        return StandardAttack(attacker, defender, damage);
    }

    public CombatMessage HeavyAttack(CombatActor attacker, CombatActor defender)
    {
        Debug.Log("Heavy!");
        return StandardAttack(attacker, defender, damage, 0, attacker.BattleData.UseAP());
    }
}
