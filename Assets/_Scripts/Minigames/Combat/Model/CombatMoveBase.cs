﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CombatMoveBase : ScriptableObject
{
    public int GetCost()
    {
        return Mathf.CeilToInt(CombatActor.SPBARPOINTS * MoveSettings.MoveCostMultiplier(settings.Cost));
    }
    public enum UsePower { Normal, Powerful, Careful}

    [System.Serializable]
    public struct TargetSettings
    {
        public enum Target { Foe, Self }
        public enum Shape { Melee, Single, Sphere, Ray}

        public Target TargetType { get { return targetType; } }
        [SerializeField]
        Target targetType;

        public Shape TargetShape { get { return targetShape; } }
        [SerializeField]
        Shape targetShape;

        public float Range { get { return range; } }
        [SerializeField]
        float range;
    }

    [System.Serializable]
    public struct MoveSettings
    {
        public enum MoveCost { Free, HalfBar, FullBar, ThreeBars}
        [SerializeField]
        MonsterStatSheet.Virtues movePower;
        public MonsterStatSheet.Virtues MovePower { get { return movePower; } }
        [SerializeField]
        MoveCost cost;
        public MoveCost Cost { get { return cost; } }
        
        public static float MoveCostMultiplier(MoveCost cost)
        {
            switch(cost)
            {
                case MoveCost.HalfBar:
                    return 0.5f;
                case MoveCost.FullBar:
                    return 1f;
                case MoveCost.ThreeBars:
                    return 3f;
            }
            return 0f;
        }
    }

    public struct CombatMessage
    {
        public enum Hit { Normal, Resist, Critical, Immune, Block, Dodge, Miss, Heal}
        public MoveDamageType damageType;
        public CombatActor target;
        public int number;
        public Hit hit;

        public void SetNumber(int number)
        {
            this.number = number;
        }

        public void SetTarget(CombatActor target)
        {
            this.target = target;
        }
    }

    [System.Serializable]
    public struct StandardDice
    {
        public int numDice;
        public Dice.DiceType dice;
        public int bonus;
    }

    [System.Serializable]
    public struct StandardDamage
    {
        public int baseDamage;
        public int bonusDamage;
        public MoveDamageType damageType;

        public int RollFail(int atkBonus)
        {
            return Mathf.CeilToInt(baseDamage * 0.5f) + atkBonus;
        }

        public int RollSuccess(int atkBonus)
        {
            return Mathf.FloorToInt(baseDamage * Random.Range(0.6f, 1.6f)) + (atkBonus * Random.Range(1, bonusDamage));
        }

        public int RollCritical(int atkBonus)
        {
            return Mathf.FloorToInt(baseDamage * 2f) + (atkBonus * bonusDamage);
        }
    }

    [SerializeField]
    string properName;
    public string ProperName { get { return properName; } }

    [SerializeField]
    [TextArea]
    string longDescription;
    public string Description { get { return string.Format("[{0}]:{1}", settings.MovePower.ToString() ,longDescription); } }

    [SerializeField]
    TargetSettings targetting;
    public TargetSettings Targetting { get { return targetting; } }

    [SerializeField]
    MoveSettings settings;
    public MoveSettings Settings { get { return settings; } }

    public virtual CombatMessage[] Execute(CombatActor attacker, CombatActor[] defenders) { return null; }

    public virtual CombatMessage Execute(CombatActor attacker, CombatActor defender) { return new CombatMessage(); }

    public virtual CombatMessage Execute(CombatActor self) { return new CombatMessage(); }

    public void PayCost(CombatActor attacker)
    {
        attacker.PaySPBar(Mathf.CeilToInt(MoveSettings.MoveCostMultiplier(settings.Cost) * CombatActor.SPBARPOINTS));
    }

    protected void DamageCard(CombatActor defender, int damage)
    {
        Debug.Log("Damage received: " + damage);
        defender.CurHP -= damage;

        if (defender.CurHP == 0) defender.BattleData.BattleStance = MonsterBattleData.Stance.KO;
    }

    protected void RaiseSP(CombatActor target, int value)
    {
        //Debug.Log("Giving " + target.cardName + " (" + value + ") SP");
        target.CurSP += value;
    }

    protected void HealCard(CombatActor defender, int heal)
    {
        defender.CurHP += heal;
    }

    protected int AttackRoll(int modifier)
    {
        int roll = Mathf.Clamp(Random.Range(1, 20) + modifier, 1, 20);
        return (roll >= 20 ? 1 : roll <= 1 ? -1 : 0);
    }

    
    protected int AttackRollAdvantage(int modifier, bool advantage)
    {
        if (advantage)
        {
            return Mathf.Max(AttackRoll(modifier), AttackRoll(modifier));
        }
        else
        {
            return Mathf.Min(AttackRoll(modifier), AttackRoll(modifier));
        }
    }

    protected void DamageAugment(CombatActor defender, CombatMessage message)
    {
        if(defender.IsWeak(message.damageType))
        {
            message.number *= 2;
            message.hit = CombatMessage.Hit.Critical;
        }

        if(defender.IsResist(message.damageType))
        {
            message.number = Mathf.CeilToInt(message.number * 0.5f);
            message.hit = CombatMessage.Hit.Resist;
        }

        if(defender.IsImmune(message.damageType))
        {
            message.number = 0;
            message.hit = CombatMessage.Hit.Immune;
        }
    }

    protected CombatMessage StandardAttack(CombatActor attacker, CombatActor defender, StandardDamage damage, int successMod = 0, int powerMod = 0)
    {
        CombatMessage _return = new CombatMessage();
        _return.damageType = damage.damageType;
        int success = AttackRoll(successMod);
        _return.SetTarget(defender);
        int pow = attacker.GetVirtueValue(settings.MovePower) + powerMod;
        int value = Mathf.CeilToInt((success ==  1 ? damage.RollCritical(pow) : success == 0 ? damage.RollSuccess(pow) : damage.RollFail(pow)));
        value = Mathf.Max(value, 1);
        _return.hit = success == 1 ? CombatMessage.Hit.Critical : CombatMessage.Hit.Normal;

        _return.number = value;

        DamageAugment(defender, _return);

        DamageCard(defender, _return.number);
        RaiseSP(attacker, Mathf.CeilToInt(_return.number * 0.6f));
        RaiseSP(defender, Mathf.CeilToInt(_return.number * 0.3f));
        if (Settings.Cost == 0) RaiseSP(attacker, Mathf.CeilToInt(_return.number * 1.5f));

        return _return;
    }
}
