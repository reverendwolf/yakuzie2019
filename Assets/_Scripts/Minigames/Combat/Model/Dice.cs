﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dice
{
    public enum DiceType { d4 = 4, d6 = 6, d8 = 8, d10 = 10, d12 = 12}
    
    public static int Roll(DiceType dice)
    {
        return Random.Range(0, (int)dice) + 1;
    }

    public static int Roll(int num, DiceType dice)
    {
        int result = 0;
        for (int i = 0; i < num; i++)
        {
            result += Roll(dice);
        }
        return result;
    }

    public static int RollMax(int num, DiceType dice)
    {
        return (int)dice * num;
    }

    public static int RollCritical(int num, DiceType dice)
    {
        return Roll(num * 2, dice);
    }

    public static int Roll(int num, DiceType dice, int bonus)
    {
        return Mathf.Max(Roll(num, dice) + bonus, 1);
    }

    public static int RollCritical(int num, DiceType dice, int bonus)
    {
        return Mathf.Max(Roll(num * 2, dice) + (bonus * 2), 1);
    }
    

    public static int RollSkill(int bonus)
    {
        return Mathf.Max(Random.Range(1, 21) + bonus, 1);
    }

    public static int RollAttack()
    {
        return RollSkill(0);
    }

    public static int RollSkillAdvantage(int bonus, bool advantage)
    {
        
        if(advantage)
        {
            return Mathf.Max(RollSkill(bonus), RollSkill(bonus));
        }
        else
        {
            return Mathf.Min(RollSkill(bonus), RollSkill(bonus));
        }
    }

    public static int RollPercentile()
    {
        return Random.Range(1, 101);
    }
}
